<?php
//error_reporting(0);
    require_once ('../../../../Public/library/jpgraph/jpgraph-3.5.0b1/src/jpgraph.php');
	require_once ('../../../../Public/library/jpgraph/jpgraph-3.5.0b1/src/jpgraph_pie.php');
	require_once ('../../../../Public/library/jpgraph/jpgraph-3.5.0b1/src/jpgraph_pie3d.php');
	require_once ('../../../../Public/library/jpgraph/jpgraph-3.5.0b1/src/jpgraph_line.php');
	require_once ('../../../../Public/library/jpgraph/jpgraph-3.5.0b1/src/jpgraph_bar.php');
	
ini_set("memory_limit", "-1"); 
ini_set("max_execution_time", '0');
?>
<?php
require_once('../../../../Public/Connections/projector_system_XZ_MIM_web.php'); 
//$item1 = $_REQUEST['item1'];
$item2 = $_REQUEST['item2'];
$item3 = $_REQUEST['item3'];
$Ave=array();
$measureC=array();
$OpC=array();
$date1=$item3.'-01-01 00:00:00';
$date2=$item3.'-12-31 23:59:59';	
	$counter = 1;
	$legendname_show = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	$legendname_show1 = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	$init_data = array_fill(0,12,0);
	$targ = array_fill(0,12,"#");
	$targ1 = array_fill(0,12,"#");
	$totaldata = array_combine($legendname_show,$init_data);
	$data = array_combine($legendname_show,$init_data);
	$totaldata1 = array_combine($legendname_show,$init_data);
	$data1 = array_combine($legendname_show,$init_data);
	$finaltotaldata = array();
	$finaldata = array();
	$finaltotaldata1 = array();
	$finaldata1 = array();
	$counter=0;
	$cstr=""; 
 mysqli_select_db($connect,$database );
 $cavity_array=array('mim_measure_data_1cav','mim_measure_data_2cav','mim_measure_data_4cav','mim_measure_data_8cav','mim_measure_data_16cav');
 
 $OpC=array_fill_keys($legendname_show,"0");
 $measureC=array_fill_keys($legendname_show,"0");	
 $Ave=array_fill_keys($legendname_show,"0");
foreach($cavity_array as $cav)
{
$sql0 = "SELECT COUNT( `FAI_Number` ) AS T , COUNT( DISTINCT  `Personal_ID` ) AS OPCount, SUBSTR(MONTHNAME( Measure_Datetime ), 1, 3 ) AS Mon FROM  $cav WHERE 1 = 1 AND Measure_Datetime Between '$date1' AND '$date2' GROUP BY Mon   order by Measure_Datetime ";
 $OPCount = mysqli_query($connect,$sql0) ;  

while($OPCounts = mysqli_fetch_assoc($OPCount))
{
$measureC[$OPCounts['Mon']]+=$OPCounts['T'];
$OpC[$OPCounts['Mon']]+=$OPCounts['OPCount'];

}
}
foreach($measureC as $k => $val)
{if($OpC[$k]>0)
$Ave[$k]=$measureC[$k]/$OpC[$k];
}

	 echo "<BR>";
	 echo "以下文字顯示---<font color=\"blue\">當月量測批號數量<font color=\"red\"><strong>/</strong></font>當月部門人數</font>---做為參考平均值";
	echo "<BR>";
foreach($Ave as $child => $v) {
   echo $child;
   echo " is ";
   echo "<font color=\"blue\">".$v."</font>";
   echo "  ****  ";
}	 echo "<BR>";
	 echo "<BR>";
	 echo "<BR>"; 
	function cmp($a, $b)
	{
	     if ($a == $b) 
	     {
	         return 0;
	     }
	     return ($a < $b) ? -1 : 1;
	}
  
  function makeSortFunction($field)
  {
    $code = "return cmp(\$a['$field'], \$b['$field']);";
    return create_function('$b,$a', $code);
  }
	

$cnt=0;	
foreach($cavity_array as $cav)
{
	
//person per month	
  $sql1 = "SELECT  COUNT( `FAI_Number` ) as Count , SUBSTR(MONTHNAME( Measure_Datetime ) , 1, 3 ) AS Mon FROM  $cav WHERE 1 =1 AND `Personal_ID` =  '".$item2."' AND Measure_Datetime Between '$date1' AND '$date2' GROUP BY Mon ORDER BY  Measure_Datetime ASC ";	
 // echo $sql1;
	$row1 = mysqli_query($connect,$sql1 ); 	
//	$item_total1 = $item_total1+ mysqli_num_rows($row1);
while($rows1 = mysqli_fetch_assoc($row1))
			{
					$data[$rows1['Mon']] += $rows1['Count'];		//個人每個月量測尺寸數			
					$cnt++;
			}
}
	$uploadtotal = 0;
	$content = "";
	$content = $content."<table align=center><tr>";
	$cnt=0;

	
	$alt = array_fill(0,12,"val=%d");
	$filename = "../../../../Report/Projector_System/HQ_MIM/img1/".rand().".png"; 
	$map_links = "map_links";


foreach($measureC as $k => $val)
	@$totaldata[$k] =$data[$k] /$measureC[$k];
	
	
	$cnt=0;
	foreach($data as $key => $value)
	{
		$finaldata[$cnt++] = $value;
	}
	
	$cnt=0;
	foreach($totaldata as $key => $value)
	{
		$finaltotaldata[$cnt++] = $value;
	}  
	
	$filename = "../../../../Report/Projector_System/HQ_MIM/img1/".rand().".png"; 
	$title = $item3."年1-12月量測批號總數統計";
	$y2title = "比分百重比門部佔";
	$ytitle = "量數號批";
	$xtitle = "月份";
	$graph = new Graph(600,300,"auto");     
	$graph->SetScale("textlin"); 
	$graph->img->SetMargin(60,60,30,40); 
	$graph->title->SetFont(FF_CHINESE, FS_NORMAL);
	$graph->title->Set($title);
	$graph->title->SetMargin(8);	
	$graph->title->SetColor("darkred");
	$graph->SetY2Scale("lin");	
	$graph->SetY2OrderBack(false);	
	
	$graph->y2axis->title->Set($y2title);
	$graph->y2axis->title->SetMargin(5); // Some extra margin to clear labels
	$graph->y2axis->title->SetFont(FF_CHINESE, FS_NORMAL);
	$graph->y2axis->SetColor('red');
	
	
  $graph->xaxis->title->SetFont(FF_CHINESE, FS_NORMAL);
	$graph->xaxis->title->Set($xtitle); 	
	$graph->xaxis->SetTickLabels($legendname_show);
	$graph->yaxis->title->SetFont(FF_CHINESE, FS_NORMAL);
	$graph->yaxis->title->SetMargin(20);
	$graph->yaxis->title->SetColor("red");
	$graph->yaxis->title->Set($ytitle); 
				
	$barplot=new barPlot($finaldata);
	$graph->Add($barplot); 
	$barplot->SetFillColor('lightblue');
	$barplot->SetColor('lightblue');
	$barplot->SetWidth(0.1);
	$barplot->SetShadow('darkgray');
	//$barplot->value->SetFormat('%d'.$unit);
	$barplot->SetValuePos('center');
	$barplot->value->SetFont(FF_CHINESE, FS_NORMAL); 
	$barplot->value->Show();
	$barplot->SetCSIMTargets($targ,$alt);
				
	$lineplot = new LinePlot($finaltotaldata);
	$graph->AddY2($lineplot);
	$lineplot->mark->SetType(MARK_STAR);
	$lineplot->SetCenter();
	$lineplot->mark->SetType(MARK_UTRIANGLE);
	$lineplot->value->show();
	$lineplot->value->SetMargin(10);
	$lineplot->SetColor("red");
	//$lineplot->SetLineWeight(0);
	$lineplot->value->SetColor('darkred');
	$lineplot->value->SetFormat('%0.2f%%');			
				
					 
	@unlink($filename);
	$graph->Stroke($filename);  						
	$imgMap = $graph->GetHTMLImageMap($map_links);

  $filename1 = "../../../../Report/Projector_System/HQ_MIM/img1/".rand().".png"; 
	$map_links1 = "map_links1";
	$cnt=0;
  $alt1 = array_fill(0,12,"val=%d");
	$cnt=0;
	
$OverallWorkTime=array_fill_keys($legendname_show,"0");	
	foreach($cavity_array as $cav)
{
	$sql = "SELECT  Time, SUBSTR(MONTHNAME( Measure_Datetime ), 1, 3 ) AS Mon,  Personal_ID FROM  $cav WHERE 1=1 AND Measure_Datetime Between '$date1' AND '$date2' ORDER BY  Measure_Datetime ASC";
	  $rows = mysqli_query($connect,$sql ); 	
	while($row = mysqli_fetch_assoc($rows))
		$OverallWorkTime[$row['Mon']]+= $row['Time']/6000;  
}

$PersonWorkTime=array_fill_keys($legendname_show,"0");
foreach($cavity_array as $cav)
{
	$sql = "SELECT  Time, SUBSTR(MONTHNAME( Measure_Datetime ), 1, 3 ) AS Mon FROM  $cav WHERE 1=1 and `Personal_ID` =  '".$item2."'  AND Measure_Datetime Between '$date1' AND '$date2' ORDER BY  Measure_Datetime ASC";
	  $rows = mysqli_query($connect,$sql ); 	
	while($row = mysqli_fetch_assoc($rows))
		$PersonWorkTime[$row['Mon']]+= $row['Time']/6000;  
}	
	
	foreach($PersonWorkTime as $k => $val)
	$data1[$k]=$PersonWorkTime[$k];
	
	foreach($PersonWorkTime as $k => $val)
	{
	if($OverallWorkTime[$k]>0)
	$ratedata1[$k]=$PersonWorkTime[$k]*100/$OverallWorkTime[$k];
	else
	$ratedata1[$k]=$PersonWorkTime[$k]*100/0.000001;
	}
	$cnt=0;
	foreach($data1 as $key => $value)
	{
		$finaldata1[$cnt++] = $value;
	}
	$cn=0;
	foreach($ratedata1 as $person)
	{
		$rate[$cn++]=$person;
		}
		//print_r($data1);
		//print_r($finaldata1);
		//print_r($rate);
	//print_r($finaldata1);	
	$title1 = $item3."年1-12月量測工時總數統計(單位:分鐘)";
	$ytitle1 = "時工";
	$xtitle1 = "月份";
	$graph1 = new Graph(600,300,"auto");     
	$graph1->SetScale("textlin"); 
	$graph1->img->SetMargin(60,60,30,40); 
	$graph1->title->SetFont(FF_CHINESE, FS_NORMAL);
	$graph1->title->Set($title1);
	$graph1->title->SetMargin(8);	
	$graph1->title->SetColor("darkred");
	$graph1->SetY2Scale("lin");	
	$graph1->SetY2OrderBack(false);	
	$y3title = "比分百重比時工月當佔";
	$graph1->y2axis->title->Set($y3title);
	$graph1->y2axis->title->SetMargin(5); // Some extra margin to clear labels
	$graph1->y2axis->title->SetFont(FF_CHINESE, FS_NORMAL);
	$graph1->y2axis->SetColor('red');
	
  $graph1->xaxis->title->SetFont(FF_CHINESE, FS_NORMAL);
	$graph1->xaxis->title->Set($xtitle1); 	
	$graph1->xaxis->SetTickLabels($legendname_show1);
	$graph1->yaxis->title->SetFont(FF_CHINESE, FS_NORMAL);
	$graph1->yaxis->title->SetMargin(20);
	$graph1->yaxis->title->Set($ytitle1); 
				
	$barplot1=new barPlot($finaldata1);
	$graph1->Add($barplot1); 
	$barplot1->SetFillColor('lightblue');
	$barplot1->SetColor('lightblue');
	$barplot1->SetWidth(0.1);
	$barplot1->SetShadow('darkgray');
	//$barplot1->value->SetFormat('%d'.$unit);
	$barplot1->SetValuePos('center');
	$barplot1->value->SetFont(FF_CHINESE, FS_NORMAL); 
	$barplot1->value->Show();
	$barplot1->SetCSIMTargets($targ1,$alt1);
	
	$lineplot1 = new LinePlot($rate);	
	$graph1->AddY2($lineplot1);
	$lineplot1->mark->SetType(MARK_STAR);
	$lineplot1->SetCenter();
	$lineplot1->mark->SetType(MARK_UTRIANGLE);
	$lineplot1->value->show();
	$lineplot1->value->SetMargin(10);
	$lineplot1->SetColor("red");
	//$lineplot1->SetLineWeight(0);
	$lineplot1->value->SetColor('darkred');
	$lineplot1->value->SetFormat('%0.2f%%');						
	@unlink($filename1);
	$graph1->Stroke($filename1);  						
	$imgMap1 = $graph1->GetHTMLImageMap($map_links1);
	echo $imgMap1;	
?>

<body>
<form name=upload>			
<?php		
		echo $content;
		//echo $content1;
?>
<table>
	<tr><td valign=top><img src="<?php echo $filename;?>" ISMAP USEMAP="#"<?php echo $map_links;?>" border="0" /></td></tr>
	<tr><td valign=top><img src="<?php echo $filename1;?>" ISMAP USEMAP="#"<?php echo $map_links1;?>" border="0" /></td></tr>
</table>
<p align="center"><input type="button" VALUE="關閉視窗" onClick="window.close()"></p>
<BR>
<p align=left>
資料來源：量測機台的檔案<BR>

</p>
</form>
</body>
</html>