<?php
/*
* Copyright (C) 2015 Foxlink SCBG CAE Dept. All rights reserved
* Project: Projector System
* File Name: Index_Left
* Function: Function List
* Author: Angel Wang
* --------------------------------------------------
* Rev: 1.2 Date: PM 04:34 2015/01/29 Modifier: Angel Wang
* --------------------------------------------------
*/
//error_reporting(0);
session_start(); 

?>
<!DOCTYPE html>
<html >
<head>
<meta charset="utf-8" />
<title>Untitled Document</title> <link rel="stylesheet" type="text/css" href="CSS/IndexLeft_CSS.css?id='swda4'"/>
<link rel="stylesheet" href="CSS/jquery-ui.css"/>
   

	
<style type="text/css">

body { 
/*  background-repeat: repeat; */
 background-image:url(images/bg.png);
background-repeat:repeat;

/*background:#E3EDFF;*/
}

</style>

<script type="text/javascript">

function hyouji(obj) {
    obj_style = document.getElementById(obj).style;
    obj_style.display = obj_style.display != 'block' ? 'block' : 'none';
}

parent.document.getElementById("a2").addEventListener('click', function(e){ 
var attr = parent.document.getElementById("bottom_frameset").getAttribute("cols");
if (attr.split(',')[0]>200)
{
parent.document.getElementById("bottom_frameset").setAttribute("cols","1,*");
parent.document.getElementById("a2").style.backgroundImage="url(images/b3.jpg)";
}
else
{
parent.document.getElementById("bottom_frameset").setAttribute("cols","240,*");
parent.document.getElementById("a2").style.backgroundImage="url(images/b2.jpg)";
}
}, false);


function changerow()

{  

	parent.document.getElementById("disappear").setAttribute("rows",".01,*");
};

function returnrow()

{  
	parent.document.getElementById("disappear").setAttribute("rows","145,*");
};





</script>

<body>
<?php 
//error_reporting(0);
require_once('../../../Public/Connections/measurement_system_management.php');
mysqli_select_db($connect,$database);

$id=$_SESSION['ID'];
$Pri="select * from projector_account where id='$id'";
$qPri= mysqli_query($connect,$Pri );
 while($rPri = mysqli_fetch_assoc($qPri))
   { 
   for($i=1;$i<=12;$i++)
   ${'projector'.$i}=$rPri['projector'.$i];
   }
     ?>

 <?php if(${'projector1'}==1) : ?>
<form method="post" action="F-Notice Board/Notice Board-1.php" target="Index_Bottom-1">
 <table width="150" cellpadding="5" cellspacing="5" frame="void" rules="groups" style="hidden" align="center">
   <tr>
     <td align=left>&nbsp;</td>
   </tr>
   <tr>
     <td align=left><input type="submit" name="Notice Board" id="Notice Board"  value="不良排行" class="BT-1" onClick="window.open('F-Notice Board/Notice Board-2.php','Index_Bottom-2'); returnrow();"/></td>
   </tr>
   <tr>      
      <td width=150 align=left>&nbsp;</td>
   </tr> 
   </table>
   </form>
 <?php endif; ?> 


<?php if(${'projector3'}==1) : ?>
<form method="post" action="F-Measure Data/Measure Data-1.php" target="Index_Bottom-1">
 <table width="150" cellpadding="5" cellspacing="5" frame="void" rules="groups" style="hidden" align="center">
   <tr>
     <td align=left><input type="submit" name="Measure Data" id="Measure Data"  value="尺寸檢視" class="BT-2" onClick="window.open('blank.php','Index_Bottom-2'); returnrow();"/></td>
   </tr>
   <tr>
      
      <td width=151 align=left>&nbsp;</td>
   </tr> 
   </table>
   </form>
   <?php endif; ?> 


   <?php if(${'projector10'}==1) : ?>
 <form method="post" action="F-Measurement/labtest14.php" target="Index_Bottom-2">
 <table width="150" cellpadding="5" cellspacing="5" frame="void" rules="groups" style="hidden" align="center">
   <tr>
     <td align=left><input type="submit" name="Measure Other Device" id="Measure Other Device"  value="Projector量測" class="BT-7" onClick="window.open('blank.php','Index_Bottom-2'); changerow();"/></td>
   </tr>
   <tr>    
      <td width=151 align=left>&nbsp;</td>
   </tr> 
   </table>
   </form>
   <?php endif; ?>
   
   
   <?php if(${'projector6'}==1) : ?>
   <form method="post" action="F-Mim Upload/Upload_Spec-1.php" target="Index_Bottom-1">
 <table width="150" cellpadding="5" cellspacing="5" frame="void" rules="groups" style="hidden" align="center">
   <tr>
     <td align=left><input type="submit" name="Upload Spec" id="Upload Spec"  value="Projector規格上傳" class="BT-3" onClick="window.open('blank.php','Index_Bottom-2'); returnrow();"/></td>
   </tr>
   <tr>  
   <td width=151 align=left>&nbsp;</td>
      </tr> 
   </table>
   </form>
   <?php endif; ?>
   
   <?php if(${'projector11'}==1) : ?>
  <form method="post" action="F-Other Device/Measure Other Device-1.php" target="Index_Bottom-1">
 <table width="150" cellpadding="5" cellspacing="5" frame="void" rules="groups" style="hidden" align="center">
   <tr>
     <td align=left><input type="submit" name="Measure Other Device" id="Measure Other Device"  value="其他量治具量測" class="BT-14" onClick="window.open('blank.php','Index_Bottom-2'); returnrow();"/></td>
   </tr>
   <tr>
      <td width=151 align=left>&nbsp;</td>
   </tr> 
   </table>
   </form>
   <?php endif; ?>
   
   
   
   <?php if(${'projector12'}==1) : ?>
   <form method="post" action="F-Upload Spec/Upload_Spec-1.php" target="Index_Bottom-1">
 <table width="150" cellpadding="5" cellspacing="5" frame="void" rules="groups" style="hidden" align="center">
   <tr>
     <td align=left><input type="submit" name="Upload Spec" id="Upload Spec"  value="其他量治規格上傳" class="BT-13" onClick="window.open('blank.php','Index_Bottom-2'); returnrow();"/></td>
   </tr>
   <tr>     
      <td width=151 align=left>&nbsp;</td>
   </tr> 
   </table>
   </form>
   <?php endif; ?>
   
   
   
   <?php if(${'projector4'}==1) : ?>
<form method="post" action="F-Production Trend/Production Trend-1.php" target="Index_Bottom-1">
 <table width="150" cellpadding="5" cellspacing="5" frame="void" rules="groups" style="hidden" align="center">
   <tr>
     <td align=left><input type="submit" name="Production Trend" id="Production Trend"  value="生產趨勢" class="BT-3" onClick="window.open('blank.php','Index_Bottom-2'); returnrow();"/></td>
   </tr>
   <tr>     
      <td width=150 align=left>&nbsp;</td>
   </tr> 
   </table>
   </form>
   <?php endif; ?>
   
   
   <?php if(${'projector5'}==1) : ?>
   <form method="post" action="F-X_Bar Chart/X_Bar Chart-1.php" target="Index_Bottom-1">
 <table width="150" cellpadding="5" cellspacing="5" frame="void" rules="groups" style="hidden" align="center">  
   <tr>
     <td align=left><input type="submit" name="X_Bar Chart" id="X_Bar Chart"  value="CPK" class="BT-7" onClick="window.open('blank.php','Index_Bottom-2'); returnrow();"/></td>
   </tr>
   <tr>  
      <td width=150 align=left>&nbsp;</td>
   </tr> 
   </table>
   </form>
   <?php endif; ?>
   
   
   <?php if(${'projector8'}==1) : ?>
<form method="post" action="F-OP Information/View_Op_info-1.php" target="Index_Bottom-1">
 <table width="150" cellpadding="5" cellspacing="5" frame="void" rules="groups" style="hidden" align="center">  
   <tr>
     <td align=left><input type="submit" name="OP Information" id="OP Information"  value="人員KPI" class="BT-9" OnClick="window.open('blank.php','Index_Bottom-2'); returnrow();"/></td>
   </tr>
   <tr>  
      <td width=260 align=left>&nbsp;</td>
   </tr> 
   </table>
   </form> 
   <?php endif; ?>

   
</dl>  
 </dl> 

</body>
</html>

