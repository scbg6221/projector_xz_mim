<?php
require_once '../../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once'../../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';
$uploaddir =  '../../../../Report\Projector_System\HQ_MIM\Other Device Spec/';
require_once('Data_RequestMeasure-3.php');
$uploadfile = $uploaddir.basename($_FILES['myfile']['name']);
error_reporting(0);
if (move_uploaded_file($_FILES['myfile']['tmp_name'], $uploadfile)) {
    echo '<div align="center" style="background-color:#FFDDAA; color:#5555FF; font-size:24px; font-weight:200">請確認以下規格內容是否有誤,若沒問題請按以下按鈕上傳</div>';
} else {
    echo "檔案有問題 \n";
}

$load_spec = PHPExcel_IOFactory::load($uploaddir.'/'.$_FILES['myfile']['name']);
$file_name=substr($_FILES['myfile']['name'],0,-5);
$get_activesheet=$load_spec->setActiveSheetIndex(0); 
$get_activesheet2=$load_spec->setActiveSheetIndexByName('產品資訊');
?>
<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="../CSS/ALL_CSS.css">
</head>



<style type="text/css">
 @import url(menuTab.css); 
 </style>

<script type="text/javascript">
function loadTab(obj,n){
	//將 Tab 標籤樣式設成 Blur 型態 
    var li=document.getElementById('ul').getElementsByTagName('li');
	for (var i=0;i<li.length;i++){
		li[i].setAttribute('id',null);
	}
	obj.parentNode.setAttribute('id','current');
	var tabsF=document.getElementById('Measure-Data-table-2');
	var rowcount=tabsF.rows.length;


    for (var i = 2 ; i < rowcount ; i++)
     {
        var srno = tabsF.rows[i].cells[4].innerHTML;
        if (n == srno)
          {
            tabsF.rows[i].style.display = '';
          }
        else
          {
            tabsF.rows[i].style.display = 'none';
          }
     }
}
function ee(){

 var obj = document.getElementById('current').getElementsByTagName("a")[0];
 loadTab(obj,'全尺寸')
}
</script>


<body onLoad="ee()">

<form  method="post" action="Upload_Spec-3.php" target="Index_Bottom-2">

<div align="center">
<input type="submit" name="submitA" id="submitA"  value="確定上傳" class="Measure-Data-BT" onClick="result()" />
</div>

<?php

$spec = array();
$Project_Name = $get_activesheet2->getCellByColumnAndRow(1, 5)->getValue();
$Mold_Number = $get_activesheet2->getCellByColumnAndRow(1, 1)->getValue();
$Cavity = $get_activesheet2->getCellByColumnAndRow(1, 6)->getValue();
$Material_Spec = $get_activesheet2->getCellByColumnAndRow(1, 4)->getValue();
$Cad_Rev = $get_activesheet2->getCellByColumnAndRow(3, 1)->getValue();

?>
<table width="1000" id='Measure-Data-table-1' border="2">
<th colspan='3' bgcolor='#CCFFFF' style='color:#66009D;font-weight: bold;'>產品資訊</th>
  <tr>
    <th width="133" align="center" valign="middle" nowrap="nowrap" style="color:#00C;"><M>產品名稱</M></th>
    <th width="133" align="center" valign="middle" nowrap="nowrap" style="color:#00C;"><M>料號</M></th>
    <th width="178" align="center" valign="middle" nowrap="nowrap" style="color:#00C;"><M>模號</M></th>
</tr>
<tr>
    <td width="133" align="center" valign="middle" nowrap="nowrap" bgcolor="#FFFFFF"><M><?php echo $Project_Name; ?></M></td>
    <td width="133" align="center" valign="middle" nowrap="nowrap" bgcolor="#FFFFFF"><M><?php echo $file_name; ?></M></td>
    <td width="133" align="center" valign="middle" nowrap="nowrap" bgcolor="#FFFFFF"><M><?php echo $Mold_Number; ?></M></td>
    </tr>
    
 <div class="Measure-Data-table-2">
<table id="Measure-Data-table-2" style="text-align:center" border="1">
<thead>
<tr>
<th colspan='5' bgcolor='#FFDDAA' style='color:#66009D;font-weight: bold;'>規格</th>
</tr>
<th><div align="center">FAI_No.</div></th>
<th><div align="center">Nominal</div></th>
<th><div align="center">ULN</div></th>
<th><div align="center">LLN</div></th>
<th><div align="center">Scale</div></th>
</thead>
  <div align="center"></div>
<div id="tabsF">
	<ul id='ul'>
		<li id="current"><a href="javascript://" onclick="loadTab(this,'全尺寸');"><span>全尺寸</span></a></li>
		<li><a href="javascript://" onclick="loadTab(this,'重要尺寸');"><span>重要尺寸</span></a></li>
		<li><a href="javascript://" onclick="loadTab(this,'cpk');"><span>cpk</span></a></li>
		<li><a href="javascript://" onclick="loadTab(this,'iqc');"><span>iqc</span></a></li>
	</ul>
</div>

<?php
$sql = 
"Delete from `xz_mim_ipqc_proj_web`.`mim_spec_main_table` where `Part_Number_V` = '".$file_name."';

INSERT INTO `xz_mim_ipqc_proj_web`.`mim_spec_main_table` (`Project_Name`, `Part_Number_V`, `Mold_Number`, `Cavity`, `Material_Spec`, `Cad_Rev`) VALUES ('$Project_Name', '$file_name', '$Mold_Number', '$Cavity', '$Material_Spec', '$Cad_Rev');";

$sql = $sql . "Delete from `xz_mim_ipqc_proj_web`.`mim_spec` where `Part_Number_V` = '".$file_name."';

INSERT INTO `xz_mim_ipqc_proj_web`.`mim_spec` (`Part_Number_V`, `FAI_Number`, `Nominal_Dim`, `Upper_Dim`, `Lower_Dim`,`Scale_Type`,`Order`) VALUES ";


foreach ($load_spec->getWorksheetIterator() as $worksheet)
{
 $sheetname= $worksheet->getTitle();
/* if($sheetname=="全尺寸") { }
 else if($sheetname=="重要尺寸") { }
 else { }*/
 //echo $worksheet->getHighestRow()."<br/>";
 
 if($sheetname!="產品資訊") 
 {
     for ($row = 2; $row <=  $worksheet->getHighestRow(); $row++) 
	 {
		 
	  $arr1 =array();

	 if(trim($worksheet->getCellByColumnAndRow(0, $row)->getValue())!="" && trim($worksheet->getCellByColumnAndRow(1, $row)->getValue())!="" && trim($worksheet->getCellByColumnAndRow(2, $row)->getValue())!="" && trim($worksheet->getCellByColumnAndRow(3, $row)->getValue())!="")
	 {
	  echo '<tr>';
	  
     for ($column = 0; $column <= 4; $column++) 
	  {
            $val =  $worksheet->getCellByColumnAndRow($column, $row)->getValue();
					
			
		 if( $column==4)
		  {
			  
		if(count($arr1)>1)
	        {
			 echo '<td>'.$sheetname.'</td>';
			} 
			  
			if($sheetname=="全尺寸")
			{
              $sheetname="ALL";
			}
			
			if($sheetname=="重要尺寸")
			{
              $sheetname="Important";
			}	
			
			array_push($arr1,$sheetname);
            
			if($sheetname=="ALL")
			{
              $sheetname="全尺寸";
			}
			
			if($sheetname=="Important")
			{
              $sheetname="重要尺寸";
			}	
			
		  }
		else
		  {
			$val =  $worksheet->getCellByColumnAndRow($column, $row)->getCalculatedValue();  //專用於抓excel計算後的值
      		if(trim($val)!='')
       		 {
				array_push($arr1,$val);
				
        		echo '<td>'.$val.'</td>'; 
		     }


          }

      }	
	  if(count($arr1)>1)
	    {
		  array_push($spec,$arr1);
	    }
	  
      echo '</tr>';
 	 }
	}//包住
 }
}
//echo $sql;
//print_r ($spec);
	  $Fork = new Fork;

  foreach($spec as $key => $value)
  {
      $ord =  $Fork -> DimNO_OrderName( $key );
      if($key==0)
	  {
		  $ord="0000";
	  }		  
	  $sql=$sql. "('".$file_name."', '".$value[0]."', '".$value[1]."', '".$value[2]."','".$value[3]."', '".$value[4]."', '".$ord."'),";
   }
 
	$sql=substr($sql,0,-1);
//echo $sql;
?>
</body>
</table>
<input type="hidden" name="sql" id="sql"  value="<?php echo $sql; ?>"/>
</form>