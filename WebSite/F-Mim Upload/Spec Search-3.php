<?php 
/*
* Copyright (C) 2015 Foxlink SCBG CAE Dept. All rights reserved
* Project: Wanhao System
* File Name: Spec Search-3
* Function: Display all SPEC in data base
* Author: Bruce Huang
* --------------------------------------------------
* Rev: 1.2 Date: PM 04:34 2015/01/28 Modifier: Bruce Huang
* --------------------------------------------------
*/
session_start();
include '../../../../Public/MainWebUI/User_Count.php';
include'../../../../Public/MainWebUI/Login_Control.php';
require_once('../../../../Public/Connections/projector_system_XZ_MIM_web.php'); 
require_once('../../../../Public/library/PHPExcel/php-excel-reader-2.21/simplexlsx.class.php');
error_reporting(0);

    //搜尋DB內SPEC專案數量
    mysqli_select_db($connect,$database);
	$query_listout_U = "SELECT Part_Number_V FROM mim_spec group by Part_Number_V ";
    $listout_U = mysqli_query($connect,$query_listout_U) or die(mysqli_error());
	$total_num = mysqli_num_rows($listout_U);
    $total_row1 = $total_num /6 ;

//重新排列SPEC專案顯示(一排顯示六個SPEC專案)
if (is_int($total_row1)){
    $i=0;
    $j=6;
    for ($k=1;$k<=$total_row1;$k++){
	${'A'.$k} = "SELECT Part_Number_V FROM mim_spec group by Part_Number_V order by `Part_Number_V` limit $i,$j";
    ${'B'.$k} = mysqli_query($connect,${'A'.$k}) or die(mysqli_error());	
	while(${'C'.$k} = mysqli_fetch_assoc(${'B'.$k})){
   	${'data'.$k}[] = ${'C'.$k}['Part_Number_V']; 
	}
	$i+=$j;
	}
	$total_row=$total_row1;
}
  else{
	$total_row2=floor($total_row1);
	$total_row3=($total_row2)+1;
    $i=0;
    $j=6;
    for ($k=1;$k<=$total_row2;$k++){
	${'A'.$k} = "SELECT Part_Number_V FROM mim_spec group by Part_Number_V order by `Part_Number_V` limit $i,$j";
    ${'B'.$k} = mysqli_query($connect,${'A'.$k}) or die(mysqli_error());	
	while(${'C'.$k} = mysqli_fetch_assoc(${'B'.$k})){
   	${'data'.$k}[] = ${'C'.$k}['Part_Number_V']; 
	}
	$i+=$j;
	}
	
	$i=($total_row2)*6;
    $j=6;
    $k=$total_row3;
	${'A'.$k} = "SELECT Part_Number_V FROM mim_spec group by Part_Number_V order by `Part_Number_V` limit $i,$j";
    ${'B'.$k} = mysqli_query($connect,${'A'.$k}) or die(mysqli_error());	
	while(${'C'.$k} = mysqli_fetch_assoc(${'B'.$k})){
   	${'data'.$k}[] = ${'C'.$k}['Part_Number_V']; 

	}
	$total_row=$total_row3;
  }

?>

<!DOCTYPE HTML><head>
<meta charset="utf-8">
<title>Untitled Document</title></head>

<style type="text/css">
body {
  font: normal medium/1.4 sans-serif;
}
table {
  border-collapse: collapse;
}
th{
  padding: 0.25rem;
  text-align: center;
  border: 1px solid #ccc;
  background: #888888;
  font-size:15px;

}
td {
  padding: 0.25rem;
  text-align: center;
  border: 1px solid #ccc;
  font-size:13px;

}
B{
	font-family:"Arial Black", Gadget, sans-serif;
	color:#00000;
	}
tbody tr:nth-child(odd) {
  background: #eee;
}
	
.ifile {
	position:absolute;opacity:0;filter:alpha(opacity=0);
	
}
</style>
</head>

<body>
<table width='1000' method="post" style="table-layout:fixed">
<thead>
<tr>
<th width='100' colspan=6>已上傳料號_版次</th>
</tr>
</thead>

<tbody>
<?php
echo "<tr>" ;
for ($k=1;$k<=$total_row;$k++){
  for($a=0;$a<6;$a++){
	echo "<td width=100% align=right>" .${'data'.$k}[$a] . "</td>";
  }
echo "</tr>" ;
}

?>
</tbody>
</table>
</body>
</html>


