﻿<?php
/*
* Copyright (C) 2015 Foxlink SCBG CAE Dept. All rights reserved
* Project: QV_DataUpload System
* File Name: Data_UploadReport-5
* Function: Data_UploadReport-5
* Author: Sanyi_Sun
* --------------------------------------------------
* Rev: 1.0 Date: PM 04:30 2016/01/04 Modifier: Sanyi_Sun
* --------------------------------------------------
*/
?>
<?php
//排序名//
class Fork{
	
	function DimNO_OrderName($AA){
		
		$DimNO_Boom=str_split($AA);
		$DimNO_Boom1='';
		$DimNO_Boom2='';
		
		for($N=0 ; $N<strlen($AA) ; $N++ ){
			if (is_numeric($DimNO_Boom[$N])){
				$DimNO_Boom1=$DimNO_Boom1.$DimNO_Boom[$N];
			}else{
				if (!$DimNO_Boom1 == ''){
					$DimNO_Boom2=$DimNO_Boom2.str_pad($DimNO_Boom1,4,"0",STR_PAD_LEFT).$DimNO_Boom[$N];
					$DimNO_Boom1='';
				}else{
					$DimNO_Boom2=$DimNO_Boom2.$DimNO_Boom[$N];
				};
			};
		};
		if (!$DimNO_Boom1 == ''){
			$DimNOOrder=$DimNO_Boom2.str_pad($DimNO_Boom1,4,"0",STR_PAD_LEFT);
		}else{
			$DimNOOrder=$DimNO_Boom2;
		};
		$DimNOOrder=strtolower($DimNOOrder);
		
		
		return $DimNOOrder;
	}
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>DataUpload-5</title>
</head>
</html>
