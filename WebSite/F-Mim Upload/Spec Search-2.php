<?php 
/*
* Copyright (C) 2015 Foxlink SCBG CAE Dept. All rights reserved
* Project: Wanhao System
* File Name: Spec Search-2
* Function: Part Number List
* Author: Bruce Huang
* --------------------------------------------------
* Rev: 1.2 Date: PM 04:34 2015/01/28 Modifier: Bruce Huang
* --------------------------------------------------
*/
session_start();
include '../../../../Public/MainWebUI/User_Count.php';
include'../../../../Public/MainWebUI/Login_Control.php';
require_once('../../../../Public/Connections/projector_system_XZ_MIM_web.php'); 
error_reporting(0);

    $a=$_SESSION['Part_Number_VF'];
    mysqli_select_db($connect,$database);
    $query_listout_S  = "SELECT * FROM mim_spec WHERE Part_Number_V = '$a' order by `Order`";
    $listout_S = mysqli_query($connect,$query_listout_S) or die(mysqli_error());

?>

<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>

<style type="text/css">
body {
  font: normal medium/1.4 sans-serif;
}
table {
  border-collapse: collapse;
}
th{
  padding: 0.25rem;
  text-align: center;
  border: 1px solid #ccc;
  background: #888888;
  font-size:15px;

}
td {
  padding: 0.25rem;
  text-align: center;
  border: 1px solid #ccc;
  font-size:13px;

}
B{
	font-family:"Arial Black", Gadget, sans-serif;
	color:#00000;
	}
tbody tr:nth-child(odd) {
  background: #eee;
}
	
.ifile {
	position:absolute;opacity:0;filter:alpha(opacity=0);
	
}
</style>

</head>
<body>
   <tr>  
      <td >
      <input type="submit"  value="返回查詢已上傳料號"  OnClick="window.open('Spec Search-3.php','Index_Bottom-2')">
      </td>
   </tr> 

<table width='99%' method="post" style="table-layout:fixed">
<thead>
<th width='19.8%'>FAI</th>
<th width='19.8%'>尺寸</th>
<th width='19.8%'>上限</th>
<th width='19.8%'>下限</th>
<th width='19.8%'>量測型態</th>
</thead>
<tbody>

<?php
while($listoutS = mysqli_fetch_assoc($listout_S))
	 {		 
  echo "<tr>";
  echo "<td align=center>" . $listoutS['FAI_Number'] . "</td>";
  echo "<td align=center>" . $listoutS['Nominal_Dim'] . "</td>";
  echo "<td align=center>" . $listoutS['Upper_Dim'] . "</td>";
  echo "<td align=center>" . $listoutS['Lower_Dim'] . "</td>";
  echo "<td align=center>" . $listoutS['Scale_Type'] . "</td>";
  echo "</tr>";
	 }
?>


</tbody>
</table>
</body>
</html>


