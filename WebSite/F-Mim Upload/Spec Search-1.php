﻿<?php
/*
* Copyright (C) 2015 Foxlink SCBG CAE Dept. All rights reserved
* Project: Wanhao System
* File Name: Spec Search-1
* Function: GUI of Spec Search
* Author: Bruce Huang
* --------------------------------------------------
* Rev: 1.2 Date: PM 04:34 2015/01/28 Modifier: Bruce Huang
* --------------------------------------------------
*/
session_start();
include '../../../../Public/MainWebUI/User_Count.php';
include'../../../../Public/MainWebUI/Login_Control.php';
require_once('../../../../Public/Connections/projector_system_XZ_MIM_web.php'); 
error_reporting(0);

if (isset($_POST['Part_Number_VF'])) {
     $_SESSION['Part_Number_VF']=$_POST['Part_Number_VF'];
} 

//執行轉跳
if (isset($_POST['Submit_Search'])) {
	 header('Location: Spec Search-2.php');
} 

?>

<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>

<script>
var $j = jQuery.noConflict(); //解決不同版本問題

$j(document).ready(function(){
$("#Part_Number_VF").autocomplete("Spec Search_Part Number.php", {
        selectFirst: true
  });


})

</script>

<script type="text/javascript">
function result()
{
    document['form1'].action = "Spec Search-1.php";
    document['form1'].target = 'Index_Bottom-2';
}

</script>

<style>
.BT{
	font:bold;
	font:,"Arial Black", Gadget, sans-serif;
	border:#009;
	border:1px;
	border-radius:3px;
	height:25px;
	width:70px;
	background-color:#EEAEEE;
	color:#000000;
	font-size:16px;
	border-style:solid;
	cursor:pointer;
	}
	BR{height:25px;}

L{
	font-family:"Arial Black", Gadget, sans-serif;
	color:#000000;
	}

.textcss-1 {
    /* Size and position */
    position: relative; z-index: 100000;
    width: 180px;
	height:25px;
	margin-left:1px;
	
    /* Styles */
    background:#EEAEEE;
    border-radius: 2px;
    border: 1px;
	border-color:#000000;
	border-style:solid;
    box-shadow:#F99
    cursor: pointer;
    outline: none;
    /* Font settings */
    font-weight: bold;
    color:#000000;
}

.ifile {
	position:absolute;opacity:0;filter:alpha(opacity=0);
	
	}
</style>
</head>

<body>
  <form name="form1" enctype="multipart/form-data" method="post">
  <table width="500" cellpadding="5" cellspacing="5" frame="void" rules="groups" align="left">
  <tr>
 <td width=350 height=30 align=left colspan="2">
  <strong>規格書搜尋</strong></td>
 </tr>
 
 <tr>
  <td width=85 height=30 align=left>
  <strong>料號:</strong></td>
 
  <td width=175 height=30 align=left>
    <input type="text" name="Part_Number_VF" id="Part_Number_VF" class="textcss-1" value="<?php echo $_POST['Part_Number_VF'] ?>"></td>

  
  <td width=188 height=30 align=left>
    <input type="Submit" name="Submit_Search" id="Submit_Search" value="搜尋" class="BT" onclick="result()"></td>
  
  </tr>
  






</table>
</form>
</body>
</html>
