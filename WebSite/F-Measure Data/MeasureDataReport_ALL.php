<?php
require_once('../../../../Public/Connections/projector_system_XZ_MIM_web.php'); 
require_once('../../../../Public/Connections/projector_system_Oracle.php'); 
require_once '../../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once'../../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';

//尺寸報表打包
//error_reporting(0);

ini_set("memory_limit", "-1"); 
ini_set("max_execution_time", '0');
$Mold1 = $_REQUEST['Mold'];
$Part1 = $_REQUEST['Part'];
$Specc = $_REQUEST['Spec'];
$cav = $_REQUEST['Cavity'];
$d1 = $_REQUEST['Date1'];
$d2=date('Y-m-d',strtotime($d1)+60*60*24);		

//error_reporting(E_ERROR | E_PARSE);
mysqli_select_db($connect,$database);

$queryTicket = "SELECT * FROM  `mim_main_table` WHERE  Mold_Number =  '$Mold1' AND Part_Number_V = '$Part1' AND Spec_Status = 'ALL' AND CONCAT( End_Date,  '-', End_time ) BETWEEN  '".$d1."-08:00:00'AND  '".$d2."-08:00:00' order by CONCAT( End_Date, '-', End_time )";

$QTicket = mysqli_query($connect,$queryTicket) ;			
		while($ResultTicket = mysqli_fetch_assoc($QTicket) )
		{   					
        $t=0;
		unset($DataAll);
		$P=strrpos($ResultTicket['Part_Number_V'],'_');
		$Part=substr($ResultTicket['Part_Number_V'],0,$P);
	    $Version=substr($ResultTicket['Part_Number_V'],$P+1);
			$Time=$ResultTicket['End_Time'];
		    $Cavity_table= 'm_measure_data_'.$ResultTicket['Cavity'].'cav';
			$QData="SELECT * FROM ".$Cavity_table." WHERE Ticket_Number = '".$ResultTicket['Ticket_Number']."' order by `FAI_Number`,`Cavity_Number`";				
			$cav=$ResultTicket['Cavity'];
			$ResultData = mysqli_query($connect,$QData); 
			while($Result = mysqli_fetch_assoc($ResultData))
			{			    
			    $OP=$Result['Personal_ID'];
				
				if($Result['Measure_Datetime']=='0000-00-00 00:00:00')				
				{
					$MeasureValue='';
					$Measure_Datetime='';
					$Measure_Result='';
				}//if($Result['Measure_Datetime']=='0000-00-00 00:00:00')
				else
				{
					$MeasureValue=$Result['Measure_Value'];
					$Measure_Datetime=$Result['Measure_Datetime'];
					$Measure_Result=$Result['Measure_Result'];	
				}//else					
				if($t==0)//the beginning of a FAI
				{
				unset($Data_Array);
				unset($Data_Array1);// 1,2 for 16cavs, see line78
				unset($Data_Array2);
				unset($Data_Array3);// 1,2 for 16cavs, see line78
				unset($Data_Array4);
		   		$Data_Array=array();
				$Data_Array=array($Result['FAI_Number'], $Result['Nominal_Dim'], $Result['Upper_Dim'],$Result['Lower_Dim']);
				}//if($t==0)
				array_push($Data_Array,$MeasureValue);
				$t++;
				if($t==($cav))  //finished collecting all datas of all cavities
				{    
					$last=4+$cav;
					$OK=0;
					for($i=4;$i<$last;$i++)
					{
						if($Data_Array[$i]<=$Data_Array[2] & $Data_Array[$i]>=$Data_Array[3] or  $Data_Array[$i]=='' )
						$OK++;
					}//for($i=4;$i<$last;$i++)
					if(count($Data_Array)>13 && count($Data_Array)<30)//16穴
					{
						$Data_Array1=array_slice($Data_Array,0,12);	
						$Data_Array2=array_slice($Data_Array,12);
						array_unshift($Data_Array2, "", "","","");	
					}
					else if(count($Data_Array)>30)//32穴
					{
						$Data_Array1=array_slice($Data_Array,0,12);	
						$Data_Array2=array_slice($Data_Array,12);
						$Data_Array3=array_slice($Data_Array,20);
						$Data_Array4=array_slice($Data_Array,28);
						array_unshift($Data_Array2, "", "","","");
						array_unshift($Data_Array3, "", "","","");
						array_unshift($Data_Array4, "", "","","");	
						}
					//if(count($Data_Array)>15)
					else
						$Data_Array1=$Data_Array;
					if($OK==$cav)
						array_push($Data_Array1,"OK");
					else
						array_push($Data_Array1,"NG");
			    	$DataAll[]=$Data_Array1; 
					if(count($Data_Array)>13 && count($Data_Array)<30)
						$DataAll[]=$Data_Array2;   
					else if(count($Data_Array)>30)
					{
						$DataAll[]=$Data_Array2;
						$DataAll[]=$Data_Array3;
						$DataAll[]=$Data_Array4;
						}	
					$t=0;			
				}//if($t==($cav))
			}//while($Result = mysqli_fetch_assoc($ResultData))
		
		$index=0;
		if($cav>8)
		{
		$row=7;
		$num=48;
		}
		else
		{
		$row=6;
		$num=45;
		} 
	  $load_report = PHPExcel_IOFactory::load("../Report_Template/Tooling_Approval".$cav.".xlsx");
      $tt= count($DataAll)/$num+1;
	   for($pp=1;$pp<=$tt;$pp++)
	   {
		   if($cav==4)
		   {
	   		if(($pp!=1) and ($pp%2==1))
	   			$index++;      	
	   		if($pp%2==0)
	   			$letter='M';
	   		else
	   			$letter='A';
		   }
		   else
		   {
			 if($pp>1)
			   $index++;
			$letter='A';   
		   }
		if($pp!=1)		
		$arrayData=array_slice($DataAll,$num*($pp-1),$num);	
		elseif($pp==1)
		$arrayData=array_slice($DataAll,0,$num);
	   
		$load_report->setActiveSheetIndex($index);	
		$get_activesheet = $load_report->getActiveSheet();
		if($arrayData)
		$get_activesheet->fromArray($arrayData, NULL, $letter.$row);
		if($cav==4)
		{
		$get_activesheet->setCellValue('C2', $ResultTicket['Project_Name']);
		$get_activesheet->setCellValue('H2', $ResultTicket['Mold_Number']);
		$get_activesheet->setCellValue('M2', $Part);
	    $get_activesheet->setCellValue('S2', $Version);
		$get_activesheet->setCellValue('V2', $d1);
	    $get_activesheet->setCellValue('H3',$ResultTicket['Quantity']);
		$get_activesheet->setCellValue('M3',$ResultTicket['Sample_Amount']);
		}
		else
		{  
		$get_activesheet->setCellValue('C2', $ResultTicket['Project_Name']);
		$get_activesheet->setCellValue('F2', $ResultTicket['Mold_Number']);
		$get_activesheet->setCellValue('I2', $Part);
	    $get_activesheet->setCellValue('N2', $Version);
		$get_activesheet->setCellValue('P2', $d1);
	   $get_activesheet->setCellValue('F3',$ResultTicket['Quantity']);
		$get_activesheet->setCellValue('I3',$ResultTicket['Sample_Amount']);    
			
			}
	   }
	  
		$filename_xlsx="Tool_Approval".$Mold1."_".$Part1."_".date('Ymd',strtotime($d1)).".xlsx";
		
/*header('Content-Disposition: attachment;filename='.$filename_xlsx.'');
header('content-transfer-encoding: binary');
$objWriter = PHPExcel_IOFactory::createWriter($load_report, 'Excel2007');
$objWriter->setPreCalculateFormulas(false);
$objWriter->save('php://output');
*/
$objWriter = PHPExcel_IOFactory::createWriter($load_report, 'Excel2007');
$objWriter->save($filename_xlsx);
$temfile="C:\wamp\www\MainWebsite\Projector_System\MIM\WebSite\F-Measure Data/".$filename_xlsx;

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$filename_xlsx.'');
header('content-transfer-encoding: binary');
header("Content-Length: " .(string)(filesize($temfile)));

//刪除存於本機的報表
unlink($filename_xlsx);

$objWriter = PHPExcel_IOFactory::createWriter($load_report, 'Excel2007');
$objWriter->save('php://output');
}