<?php 
/*
* Copyright (C) 2014 Foxlink SCBG CAE Dept. All rights reserved
* Project: Projector System
* File Name: Measure Data-2
* Function: Result of Measure Data
* Author: Angel Wang
* --------------------------------------------------
* Rev: 1.2 Date: PM 04:34 2014/05/29 Modifier: Angel Wang
* --------------------------------------------------
*/

include '../../../../Public/MainWebUI/User_Count.php';
include '../../../../Public/MainWebUI/Login_Control.php';
require_once('../../../../Public/Connections/projector_system_XZ_MIM_web.php'); //修改處
error_reporting(0);
if(!isset($_SESSION)){ session_start(); }


$b = $_REQUEST['Mold'];
$c = $_REQUEST['Part'];
$d = $_REQUEST['Date1'];
$e = $_REQUEST['Time'];
$cav_table = $_REQUEST['Cavity'];
$Table = $_REQUEST['Table'];

 
 if($Table == 1)
 {
 	$Table_Name = 'mim_main_table';
	$cav_table1 = 'mim_measure_data_'.$cav_table.'cav';
 }
 elseif($Table == 0)
  {
 	$Table_Name = 'other_device_main_table';
	$cav_table1 = 'other_device_measure_data_'.$cav_table.'cav';
 }
 if ($b)
{$qb = "AND ".$Table_Name.".Mold_Number='$b' ";}
if ($c)
{$qc = "AND ".$Table_Name.".Part_Number_V='$c' ";}
if ($d)
{$qd = "AND ".$Table_Name.".End_Date='$d' ";}
if ($e)
{$qe = "AND ".$Table_Name.".End_Time='$e' ";}

 mysqli_select_db($connect,$database );
$query_listoutF=
"SELECT	  
		".$Table_Name.".Mold_Number,
		".$Table_Name.".Part_Number_V,
		".$Table_Name.".Ticket_Number,	
		".$cav_table1.".Ticket_Number,
		".$cav_table1.".Cavity_Number,
		".$cav_table1.".FAI_Number,
	    ".$cav_table1.".Measure_Value,		
		".$cav_table1.".Nominal_Dim,
		".$cav_table1.".Upper_Dim,
		".$cav_table1.".Lower_Dim,
		".$cav_table1.".Measure_Datetime,
		".$cav_table1.".Measure_Result	
    FROM ".$Table_Name.", ".$cav_table1." WHERE 1=1  ".$qb." ".$qc." ".$qd." ".$qe."  AND ".$cav_table1.".Ticket_Number=".$Table_Name.".Ticket_Number ORDER BY Start_Date ASC,Start_Time ASC";	
  $listoutF = mysqli_query($connect,$query_listoutF ) ;
?>

<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<script src="../../../../Public/library/Other/Sorttable.js"></script>
<link rel="stylesheet" type="text/css" href="../CSS/ALL_CSS.css">
</head>
<body>
<div class="Measure-Data-table-2">
<table id="Measure-Data-table-2" class="sortable">
<thead>
<th><div align="center">模號</div></th>
<th><div align="center">料號</div></th>
<th><div align="center">穴號</div></th>
<th><div align="center">FAI號</div></th>

<th><div align="center">尺寸</div></th>
<th><div align="center">上限</div></th>
<th><div align="center">下限</div></th>
<th><div align="center">量測值</div></th>
<th><div align="center">量測時間</div></th>
<th><div align="center">判定</div></th>
</thead>
  <div align="center"></div>
<tbody>

<?php
$projector_table = 0;
while($listout = mysqli_fetch_assoc($listoutF))
	 {		
	 $projector_table = 1;
	 if	($listout['Measure_Result']=='NG')
	 $color="<font color='#EE0000'>";
	 else
	 $color="";
	 if($listout['Measure_Datetime']=='0000-00-00 00:00:00')
	 {
		 $listout['Measure_Datetime']='';
		 $listout['Measure_Value']='';
	 }
		 
 
  echo "<tr>";
  echo "<td>".$color . $listout['Mold_Number'] . "</td>";
  echo "<td>".$color . $listout['Part_Number_V'] . "</td>";
  echo "<td>".$color . $listout['Cavity_Number'] . "</td>";
  echo "<td>".$color . $listout['FAI_Number'] . "</td>";
 
  echo "<td>".$color . $listout['Nominal_Dim'] . "</td>";
  echo "<td>".$color . $listout['Upper_Dim'] . "</td>";
  echo "<td>".$color . $listout['Lower_Dim'] . "</td>";
  echo "<td>".$color . $listout['Measure_Value'] . "</td>";
  echo "<td>".$color . $listout['Measure_Datetime'] . "</td>";
  echo "<td>".$color . $listout['Measure_Result'] . "</td>";
  echo "</tr>";
  }
	 
?>
</tbody>
</table>

</body>
</html>


