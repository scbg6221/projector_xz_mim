<?php
session_start();

header('Content-Type: text/html; charset=utf-8');
require_once '../../../../Public/Connections/projector_system_XZ_MIM_web.php';
require_once '../../../../Public/Connections/measurement_system_management.php';
mysqli_select_db($connect, $database);

if (isset($_POST["scan"])) {
    $scan = $_POST["scan"];
    $turn = $_POST["turn"];

    $sql2           = "SELECT * FROM `xz_mim_ipqc_proj_web`.`mim_main_table` WHERE Ticket_Number='" . $scan . "'";
    $query_AllData2 = mysqli_query($connect, $sql2);
    $num2           = mysqli_num_rows($query_AllData2);
    if ($num2 == 0) {
        if ($turn == "disa") {
            $sql           = "SELECT * FROM `projector_oracle`.`fl_rc_tickets` WHERE Ticket_NO='" . $scan . "'";
            $query_AllData = mysqli_query($connect, $sql);
            $num           = mysqli_num_rows($query_AllData);
            if ($num > 0) {
                while ($AllData = mysqli_fetch_array($query_AllData)) {
                    $pri  = $AllData['Primary_Item_NO']; //料號
                    $mol  = $AllData['Mold_Num']; //模號
                    $abc3 = $AllData['Control_Num']; //管控編號
                    $abc4 = $AllData['Prod_Line_Code']; //機台號 (生產線
                }
            } else {
                $scan       = '';
                $pri        = '';
                $mol        = '';
                $abc        = ''; //量測階段
                $def        = ''; //量測型態
                $ghi        = ''; //工站名稱
                $ijk        = ''; //量測單位
                $ps         = ''; //備註
                $comp       = ''; //廠商
                $fireord    = ''; //爐次
                $machnum    = ''; //機台號
                $personalid = '';
                echo '<script>alert("查無條碼對應的資訊");</script>';
            }
        } else {
            if ($_POST["pri"] != "") {
                $pri            = $_POST["pri"];
                $sql3           = "SELECT * FROM `xz_mim_ipqc_proj_web`.`mim_spec_main_table` WHERE Part_Number_V='" . $pri . "'";
                $query_AllData3 = mysqli_query($connect, $sql3);
                $num3           = mysqli_num_rows($query_AllData3);
                if ($num3 == 0) {
                    $pri        = '';
                    $mol        = '';
                    $abc        = ''; //量測階段
                    $def        = ''; //量測型態
                    $ghi        = ''; //工站名稱
                    $ijk        = ''; //量測單位
                    $ps         = ''; //備註
                    $comp       = ''; //廠商
                    $fireord    = ''; //爐次
                    $machnum    = ''; //機台號
                    $personalid = '';
                    echo '<script>alert("查無此Spec,請通知管理者上傳此料號之Spec");</script>';
                } else {
                    if ($_POST["mol"] != "") {
                        $mol        = $_POST["mol"];
                        $abc        = $_POST["abc"]; //量測階段
                        $def        = $_POST["def"]; //量測型態
                        $ghi        = $_POST["ghi"]; //工站名稱
                        $ijk        = $_POST["ijk"]; //量測單位
                        $ps         = $_POST["ps"]; //備註
                        $comp       = $_POST["comp"]; //廠商
                        $fireord    = $_POST["fireord"]; //爐次
                        $machnum    = $_POST["machnum"]; //機台號
                        $personalid = $_POST["personalid"];
                    } else {
                        $mol        = '';
                        $abc        = ''; //量測階段
                        $def        = ''; //量測型態
                        $ghi        = ''; //工站名稱
                        $ijk        = ''; //量測單位
                        $ps         = ''; //備註
                        $comp       = ''; //廠商
                        $fireord    = ''; //爐次
                        $machnum    = ''; //機台號
                        $personalid = '';
                    }
                }
            } else {
                $pri        = '';
                $mol        = '';
                $abc        = ''; //量測階段
                $def        = ''; //量測型態
                $ghi        = ''; //工站名稱
                $ijk        = ''; //量測單位
                $ps         = ''; //備註
                $comp       = ''; //廠商
                $fireord    = ''; //爐次
                $machnum    = ''; //機台號
                $personalid = '';
            }
        }
    } else {
        echo '<script>alert("此票號已存在，請勿使用")</script>';
        $scan       = '';
        $pri        = '';
        $mol        = '';
        $abc        = ''; //量測階段
        $def        = ''; //量測型態
        $ghi        = ''; //工站名稱
        $ijk        = ''; //量測單位
        $ps         = ''; //備註
        $comp       = ''; //廠商
        $fireord    = ''; //爐次
        $machnum    = ''; //機台號
        $personalid = '';
    }
//echo $sql;
} else {
    $scan       = '';
    $pri        = '';
    $mol        = '';
    $abc        = ''; //量測階段
    $def        = ''; //量測型態
    $ghi        = ''; //工站名稱
    $ijk        = ''; //量測單位
    $ps         = ''; //備註
    $comp       = ''; //廠商
    $fireord    = ''; //爐次
    $machnum    = ''; //機台號
    $personalid = '';
}
//20170825開始修改
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>投影機量測介面-MIM</title>
</head>

<style type="text/css">
   T{
	color: #FF0000;
	}
	.slate select   { color: red; }
	.green   { background-color: #98bf21; }
	.semi-square
	 {
      -webkit-border-radius: 10px;
      -moz-border-radius: 10px;
      border-radius: 10px;
      width: 110px;
      font-size:30px;
      height: 35px;
     }
</style>

<script type="text/javascript">

window.onload = incValue;
function incValue()
{
  var mn=document.getElementById('0').value;
  var station="<?php echo $ghi; ?>";
  var process="<?php echo $abc; ?>";
  var measureform="<?php echo $def; ?>";
  var department="<?php echo $ijk; ?>";
  if(mn!='')
     {
	   if(station=="1"){document.getElementsByName("ghi")[0].checked=true;}
	   if(station=="7"){document.getElementsByName("ghi")[1].checked=true;}
	   if(station=="2"){document.getElementsByName("ghi")[2].checked=true;}
	   if(station=="8"){document.getElementsByName("ghi")[3].checked=true;}
	   if(station=="3"){document.getElementsByName("ghi")[4].checked=true;}
	   if(station=="9"){document.getElementsByName("ghi")[5].checked=true;}
	   if(station=="4"){document.getElementsByName("ghi")[6].checked=true;}
	   if(station=="10"){document.getElementsByName("ghi")[7].checked=true;}
	   if(station=="5"){document.getElementsByName("ghi")[8].checked=true;}
	   if(station=="11"){document.getElementsByName("ghi")[9].checked=true;}
	   if(station=="6"){document.getElementsByName("ghi")[10].checked=true;}
	   if(station=="12"){document.getElementsByName("ghi")[11].checked=true;}

	   if(process=="1"){document.getElementsByName("abc")[0].checked=true;}
	   if(process=="2"){document.getElementsByName("abc")[1].checked=true;}
	   if(process=="3"){document.getElementsByName("abc")[2].checked=true;}
	   if(process=="4"){document.getElementsByName("abc")[3].checked=true;}
	   if(process=="5"){document.getElementsByName("abc")[4].checked=true;}
	   if(process=="6"){document.getElementsByName("abc")[5].checked=true;}

	   if(measureform=="ALL"){document.getElementsByName("def")[0].checked=true;}
	   if(measureform=="Important"){document.getElementsByName("def")[1].checked=true;}
	   if(measureform=="CPK"){document.getElementsByName("def")[2].checked=true;}
	   if(measureform=="IQC"){document.getElementsByName("def")[3].checked=true;}
	   if(measureform=="PPK"){document.getElementsByName("def")[4].checked=true;}

	   if(department=="1"){document.getElementsByName("ijk")[0].checked=true;}
	   if(department=="2"){document.getElementsByName("ijk")[1].checked=true;}
     }
}



function disable(val)
{
   var x = document.getElementsByClassName('123')
   var len = x.length;
   for(var i = 0 ; i < len ; i++)
     {
     x[i].value=''
    if (x[i].id!= "0")
	 {
	   if (val == "ena")
	    {
        x[i].disabled = false;
        }
       else
	    {
         x[i].disabled = true;
		//x[i].style.backgroundColor="#9D9D9D"
     }
     }
      //alert(x[i].innerHTML)
	}
}

function samepage() //輸入完條碼，自動撈取DB顯示其他資訊
   {
     var g= document.getElementsByName('turn')

             {
			  	if (document.getElementById('0').value!='')
	               {
	                 document['form1'].action="labtest14.php"
	                 document['form1'].submit();
	                }
    }
   }
function testtext() //輸入格空白檢查，無問題後提交
{
  var a=document.getElementById("0")
  var b=document.getElementById("1")
  var c=document.getElementById("2")
  var d=document.getElementById("3")
  var e=document.getElementById("4")
  var f=document.getElementById("5")


  if(a.value==""||b.value==""||c.value==""||e.value=="")
  {
      if(a.value=="")
	   {
	    alert("條碼請勿空白!")
        return false
	   }
	   if(b.value=="")
	   {
	    alert("料號請勿空白!")
        return false
	   }
      if(c.value=="")
	   {
	    alert("模號請勿空白!")
        return false
	   }

      if(e.value=="")
	   {
	    alert("爐次請勿空白!")
        return false
	   }

  }

  var g=document.getElementById("personalid")

    if(g.value=="")
  {
	alert("人員工號請選取!")
    return false
  }



 // alert ('fuck why not?')

  var i=0,ci=-1; //檢查Form名為form1中的ratio名為abc，是否有被選取 // 6/16 update
  while(i<form1.abc.length)
     {
	  if(form1.abc[i].checked)
       {
		  ci=i;
		  break;
	   }
	   i++;
     }
  if(ci==-1)
  {
	  alert("量測階段未選取")
      return false
  }
  else
   {
	  if(ci==4||ci==5)
	  {
		 if(document.getElementById('ps').value=='')
		 {
		   alert("備註欄位需註記!")
           return false
		 }
	   }
    }

  var i=0,flag=true; //檢查Form名為form1中的ratio名為def，是否有被選取
  while(i<form1.def.length && flag){if(form1.def[i].checked){flag=false;}i++;}
  if(flag){alert("量測型態未選取")
           return false}


	var i=0,flag=true; //檢查Form名為form1中的ratio名為ghi，是否有被選取
  while(i<form1.ghi.length && flag){if(form1.ghi[i].checked){flag=false;}i++;}
  if(flag){alert("工站名稱未選取")
           return false}

		var i=0,flag=true; //檢查Form名為form1中的ratio名為ijk，是否有被選取
  while(i<form1.ijk.length && flag){if(form1.ijk[i].checked){flag=false;}i++;}
  if(flag){alert("量測單位未選取")
           return false}

			      b.disabled=false   //因disabled狀態下無法傳值，故需先解除後，再進行提交
                  c.disabled=false
                  d.disabled=false
                  e.disabled=false

                  document['form1'].action ="619datashowtest.php";
                   document['form1'].submit();

}

</script>

<body background="../../../../Public/library/Entrance/images/loginb.png">

   <br>

<form name="form1" method="post" onSubmit="return testtext()" >
   &nbsp; &nbsp; &nbsp; &nbsp;
   <label><input type="radio" name="turn" value="disa" onclick="disable(this.value)"> ERP </label>

   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   <label><input type="radio" name="turn" value="ena" onclick="disable(this.value)" checked> 手動 </label>
   &nbsp; &nbsp; &nbsp; &nbsp;



<tr>
  <td align=left>
  &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
  <strong>人員工號(<T>*</T>):</strong></td>
  &nbsp; &nbsp;
    <select name="personalid" id="personalid" class="styled-select green semi-square" style="width: 150px;" value="<?php echo $personalid ?>">
	   <?php
$select_id = "SELECT * FROM mainaccount WHERE 1=1  AND DivisionCode = '1742' order by id";
echo $select_id;
$query_id = mysqli_query($connect, $select_id) or die(mysql_error());
while ($getid = mysqli_fetch_array($query_id)) {
    $id[] = $getid['id'];
}
echo "<option>" . $personalid . "</option>";

foreach ($id as $c) {
    echo "<option value='" . $c . "'>" . $c . "</option>";
}

?>
    </select>
  </td>
</tr>

   <br><br>
   &nbsp;&nbsp; 欄位請勿輸入特殊字元如:<T> / \ * : ? " < > |</T>
   <br> <br>
<div id='div' style="height: 510px; width: 100%;">
   <div style=" height: 300px; width: 26.5%;  float:left; font-size:15px;">
       &nbsp;&nbsp;&nbsp;請刷條碼(<T>*</T>): <br>
       &nbsp;&nbsp;&nbsp;<input type="text" id="0" name="scan" onblur="samepage()" class="123" value="<?php echo $scan ?>" style="width:200px; height: 25px;">  <br><br><br>
       &nbsp;&nbsp;&nbsp;手動輸入 料號_版次(<T>*</T>): <br>
       &nbsp;&nbsp;&nbsp;<input type="text" id="1" name="pri" onblur="samepage()" class="123" value="<?php echo $pri ?>" style="width:200px; height: 25px;" ><br><br><br>
       &nbsp;&nbsp;&nbsp;手動輸入 模號(<T>*</T>): <br>
       &nbsp;&nbsp;&nbsp;<input type="text" id="2" name="mol" class="123" value="<?php echo $mol ?>" style="width:200px; height: 25px;" > <br> <br>   <br>
   </div>

  <div style=" height: 300px; width: 30%; border: solid 2px #98bf21;  float:left; line-height: 220%; font-size:20px;">
       &nbsp;&nbsp;&nbsp
       工站名稱(<T>*</T>):
       <br>
       &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="ghi" value="1"> 生胚</label>
	   &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp &nbsp;&nbsp;&nbsp;&nbsp;
	   <label><input type="radio" name="ghi" value="7"> 噴砂</label>

       <br>
       &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="ghi" value="2"> 燒結</label>
       &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="ghi" value="8"> CNC</label>

         <br>
       &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="ghi" value="3"> 整型</label>
	          &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="ghi" value="9"> 噴漆</label>

	            <br>
       &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="ghi" value="4"> 電鍍</label>
	      &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="ghi" value="10"> 髮絲</label>

         <br>
       &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="ghi" value="5"> 燒結+噴砂</label>
	      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	   <label><input type="radio" name="ghi" value="11"> 鈍化</label>

         <br>
       &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="ghi" value="6"> 研磨</label>
	   &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp &nbsp;&nbsp;&nbsp &nbsp;&nbsp;&nbsp &nbsp;&nbsp;
	   <label><input type="radio" name="ghi" value="12"> PVD</label>

   </div>
   <div style=" height: 300px; width: 30%; border: solid 2px #98bf21;  float:left; line-height: 220%; font-size:20px;">

       &nbsp;&nbsp;&nbsp
       量測階段(<T>*</T>):
       <br>
       &nbsp;&nbsp;&nbsp
        <label><input type="radio" name="abc" value="1"> 首末件量測</label>

       <br>
       &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="abc" value="2"> 巡迴檢驗</label>

         <br>
       &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="abc" value="3">  調機初件</label>

         <br>
       &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="abc" value="4"> 換料初件</label>

         <br>
       &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="abc" value="5"> 模具維修初件(需備註)</label>

         <br>
       &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="abc" value="6"> 其他(需備註)</label>

   </div>


<!--	 <div style=" height: 260px; width: 5%; border: solid 2px #98bf21;  float:left; line-height: 200%;">
123456
    <div style=" height: 110px; width: 80%; border: solid 2px #98bf21;  ">
      123456

   </div>

   </div>-->

     <div class='aa' style=" height: 260px; width:65%; float:left;">
         <div style=" height: 260px; width: 20%; border: solid 2px #98bf21;  float:left; line-height: 220%; font-size:20px;">
       &nbsp;&nbsp;&nbsp
       量測型態(<T>*</T>):
       <br>
       &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="def" value="ALL"> 全尺寸</label>

       <br>
       &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="def" value="Important"> 重要尺寸</label>

         <br>
       &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="def" value="CPK"> CPK</label>

         <br>
       &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="def" value="IQC"> IQC</label>

         <br>
       &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="def" value="PPK"> PPK</label>
   </div>
   <div style=" height: 260px; width: 20%; border: solid 2px #98bf21;  float:left; line-height: 220%; font-size:20px;">
       &nbsp;&nbsp;&nbsp
       量測單位(<T>*</T>):
       <br><br>
       &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="ijk" value="1"> 品保</label>

       <br><br>
       &nbsp;&nbsp;&nbsp
	   <label><input type="radio" name="ijk" value="2"> 製造</label>
   </div>

      <div style="  height: 260px; width: 33%;  float:left;"> <br>
       &nbsp;&nbsp;&nbsp;廠商: <br>
       &nbsp;&nbsp;&nbsp;<input type="text" id="3" name="comp" class="123" style="width:200px; height: 25px;" value="<?php echo $comp ?>"><br><br><br>
       &nbsp;&nbsp;&nbsp;爐次(<T>*</T>): <br>
       &nbsp;&nbsp;&nbsp;<input type="text" id="4" name="fireord" class="123" style="width:200px; height: 25px;" value="<?php echo $fireord ?>" ><br><br><br>
       &nbsp;&nbsp;&nbsp;機台號: <br>
       &nbsp;&nbsp;&nbsp;<input type="text" id="5" name="machnum" class="123" style="width:200px; height: 25px;" value="<?php echo $machnum ?>"> <br> <br>   <br>
      </div>



        <div style=" height: 260px; width: 20%; float:left; ">
          <br> <br> <br> <br> <br> <input type="submit" value="開 始 量 測" style="width:90%; height: 60px;">
       </div>
   </div>
   <div style="height: 260px; width: 15%;  float:left; ">
   <br><br><br>&nbsp;&nbsp;&nbsp;備註:
    <br>
  &nbsp;&nbsp; <textarea id="ps" rows="6" cols="25" name="ps"><?php echo $ps ?></textarea>
   </div>



   <div style="clear:both;"></div>
</div>
</body>
</html>
