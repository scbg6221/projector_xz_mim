<?php
require_once '../../../../Public/Connections/projector_system_XZ_MIM_web.php';
require_once '../../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once '../../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';

ini_set("memory_limit", "-1");
ini_set("max_execution_time", '0');
//error_reporting(0);

//全形轉半形array
$fstr = array(
    "０", "１", "２", "３", "４", "５", "６", "７", "８", "９",
    "ａ", "ｂ", "ｃ", "ｄ", "ｅ", "ｆ", "ｇ", "ｈ", "ｉ", "ｊ",
    "ｋ", "ｌ", "ｍ", "ｎ", "ｏ", "ｐ", "ｑ", "ｒ", "ｓ", "ｔ",
    "ｕ", "ｖ", "ｗ", "ｘ", "ｙ", "ｚ",
    "Ａ", "Ｂ", "Ｃ", "Ｄ", "Ｅ", "Ｆ", "Ｇ", "Ｈ", "Ｉ", "Ｊ",
    "Ｋ", "Ｌ", "Ｍ", "Ｎ", "Ｏ", "Ｐ", "Ｑ", "Ｒ", "Ｓ", "Ｔ",
    "Ｕ", "Ｖ", "Ｗ", "Ｘ", "Ｙ", "Ｚ",
    "　",
);
$hstr = array(
    "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
    "a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
    "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
    "u", "v", "w", "x", "y", "z",
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
    "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
    "U", "V", "W", "X", "Y", "Z",
    " ",
);
//全形轉半形
$Project_Name0  = str_replace($fstr, $hstr, $_GET['Project_Name']);
$Part_Number_V0 = str_replace($fstr, $hstr, $_GET['Part_Number_V']);
$Mold_Number0   = str_replace($fstr, $hstr, $_GET['Mold_Number']);
@$Furnace_No0   = str_replace($fstr, $hstr, $_GET['Furnace_No']);

//強制轉大寫
$Project_Name  = strtoupper($Project_Name0);
$Part_Number_V = strtoupper($Part_Number_V0);
$Mold_Number   = strtoupper($Mold_Number0);
$Furnace_No1   = strtoupper($Furnace_No0);
$Furnace_No    = iconv("utf-8", "big5", $Furnace_No1);
$Department    = $_GET['Department'];
$Spec_Status   = $_GET['Spec_Status'];
$Furnace_M     = $_GET['Furnace_M'];
$Molding_M     = $_GET['Molding_M'];
$Station       = $_GET['Station'];
$Machine_Num   = $_GET['Machine_Num'];
$End_Date      = date("Y-m-d");

//2維array排序的函數
function score_sort($a, $b)
{
//利用array中"Order欄位"排序
    if ($a['0'] == $b['0']) {
        return 0;
    }
    return ($a['0'] > $b['0']) ? 1 : -1;
}
//搜尋當日同一機種有幾個票號
$queryTicket = "SELECT *,(SELECT count(Ticket_Number) FROM  `mim_main_table` WHERE  Mold_Number =  '" . $Mold_Number . "' AND Part_Number_V = '" . $Part_Number_V . "' AND Spec_Status = '" . $Spec_Status . "' AND Department = '" . $Department . "' AND End_Date=  '" . $End_Date . "' AND Furnace_No=  '" . $Furnace_No1 . "') as count,Ticket_Number,Cavity,End_Time,End_Date FROM  `mim_main_table` WHERE  Mold_Number =  '" . $Mold_Number . "' AND Part_Number_V = '" . $Part_Number_V . "' AND Spec_Status = '" . $Spec_Status . "' AND Department = '" . $Department . "' AND End_Date=  '" . $End_Date . "' AND Furnace_No=  '" . $Furnace_No1 . "' ";

$t            = 0;
$measure_time = 0;
$DataAll      = array();
mysqli_select_db($connect, $database);
$QTicket = mysqli_query($connect, $queryTicket);
while ($ResultTicket = mysqli_fetch_assoc($QTicket)) {
    $measure_time++;
    $Remark[]     = $ResultTicket['Remark'];
    $Time         = $ResultTicket['End_Time'];
    $Ticket       = $ResultTicket['Ticket_Number'];
    $count        = $ResultTicket['count'];
    $Status       = $ResultTicket['Status'];
    $Cavity_table = 'mim_measure_data_' . $ResultTicket['Cavity'] . 'cav';

    //依據每一個票號抓取數據
    $QData = "SELECT * FROM " . $Cavity_table . " WHERE Ticket_Number = '" . $ResultTicket['Ticket_Number'] . "' order by `Position`,`FAI_Number`,`Cavity_Number`";

    unset($DataAll);
    mysqli_select_db($connect, $database);
    $ResultData = mysqli_query($connect, $QData);

    while ($Result = mysqli_fetch_assoc($ResultData)) {

        if ($Result['Personal_ID'] != '') {
            $Personal_ID = $Result['Personal_ID'];
        }
        $cav = $ResultTicket['Cavity'];

        if ($Result['Measure_Datetime'] == '0000-00-00 00:00:00') {
            $MeasureValue     = '';
            $Measure_Datetime = '';
            $Measure_Result   = '';
        } //if($Result['Measure_Datetime']=='0000-00-00 00:00:00')
        else {
            $MeasureValue     = $Result['Measure_Value'];
            $Measure_Datetime = $Result['Measure_Datetime'];
            $Measure_Result   = $Result['Measure_Result'];
        } //else
        if ($t == 0) //the beginning of a FAI
        {
            unset($Data_Array);
            unset($Data_Array1);
            unset($Data_Array2);
            $Data_Array         = array();
            $MeasureValue_Array = array();

            //將FAI號碼後面串Position
            if ($Result['Position'] == '') {
                $FAI_Number = $Result['FAI_Number'];
                $Order      = $Result['Order'];
            } else {
                $FAI_Number = $Result['FAI_Number'] . "_" . $Result['Position'];
                $Order      = $Result['Order'];
            }

            $Data_Array = array($Order, $Time, $FAI_Number, $Result['Nominal_Dim'], $Result['Upper_Dim'], $Result['Lower_Dim']);

        } //if($t==0)

        array_push($Data_Array, $MeasureValue); //將$MeasureValue加到$Data_Array之後

        if ($MeasureValue != "") {
            array_push($MeasureValue_Array, $MeasureValue); //將$MeasureValue加到$MeasureValue_Array之後
        }

        $t++;

        if ($t == ($cav)) {
            $last = 6 + $cav;
            $OK   = 0;
            $Null = 0;

            //計算Upper_Percentage & Lower_Percentage
            @$Max_MeasureValue = max($MeasureValue_Array);
            @$Min_MeasureValue = min($MeasureValue_Array);
            $Avg               = ($Data_Array[4] + $Data_Array[5]) / 2;

            if ($Max_MeasureValue == 0 & $Min_MeasureValue == 0) {
                $Upper_Percentage = '';
                $Lower_Percentage = '';
            } else {
                if ($Data_Array[5] == 0) {

                    @$Upper_Percentage = $Max_MeasureValue * 100 / $Data_Array[4];
                    @$Lower_Percentage = $Min_MeasureValue * 100 / $Data_Array[4];
                } else {

                    @$Upper_Percentage = ($Max_MeasureValue - $Avg) * 100 / (($Data_Array[4] - $Data_Array[5]) / 2);
                    @$Lower_Percentage = ($Min_MeasureValue - $Avg) * 100 / (($Data_Array[4] - $Data_Array[5]) / 2);
                }
            }

            for ($i = 6; $i < $last; $i++) {
                if ($Data_Array[$i] == '') {
                    $Null++;
                } else if ($Data_Array[$i] <= $Data_Array[4] && $Data_Array[$i] >= $Data_Array[5]) {
                    $OK++;
                }
            }

            //將量測結果& Upper_Percentage & Lower_Percentage放置$Data_Array
            $add_OKNull = $OK + $Null;
            if ($Null == $cav) {
                array_push($Data_Array, "", $Upper_Percentage, $Lower_Percentage);
            } else {
                if ($add_OKNull == $cav) {
                    array_push($Data_Array, "OK", $Upper_Percentage, $Lower_Percentage);
                } else {
                    array_push($Data_Array, "NG", $Upper_Percentage, $Lower_Percentage);
                }
            }
            $DataAll[] = $Data_Array;
            $t         = 0;
        } //if($t==($cav))
    } //while($Result = mysqli_fetch_assoc($ResultData))

    //將2維array利用Order欄位做排序
    usort($DataAll, 'score_sort');

    //去除array中"Order"欄位
    $num = 8 + $cav;
    foreach ($DataAll as $k => $New_DataAll) {
        $DataAll[$k] = array_slice($New_DataAll, 1, $num);
    }

    //抓取Material_Spec資訊
    $Info = "SELECT * FROM `mim_spec_main_table` WHERE `Part_Number_V` ='" . $Part_Number_V . "'";
    mysqli_select_db($connect, $database);
    $InformationF2 = mysqli_query($connect, $Info);
    $Information2  = mysqli_fetch_assoc($InformationF2);
    $Material_Spec = $Information2['Material_Spec'];

    $index = 1; //起始頁碼
    if ($cav < 8) {
        $row = 6; //報表量測數據放置起始位子
        $num = 45; //報表每一頁能放置量測數據的數量
    } else {
        $row = 6;
        $num = 48;
    }

    //報告樣板依據$Station & $Cav切換
    //"製造單位"使用"整形"量測最多可量8次
    if ($Station == 3 && $Department == 2) {
        if ($cav == 1) {
            $Template = "../Report_Template/1cav-2.xlsx";
        } else if ($cav == 2) {
            $Template = "../Report_Template/2cav-2.xlsx";
        } else if ($cav == 4) {
            $Template = "../Report_Template/4cav-2.xlsx";
        } else if ($cav == 5) {
            $Template = "../Report_Template/5cav-2.xlsx";
        } else if ($cav == 6) {
            $Template = "../Report_Template/6cav-2.xlsx";
        } else if ($cav == 8)  {
            $Template = "../Report_Template/8cav-2.xlsx";
        } else {
            $Template = "../Report_Template/9cav-2.xlsx";
        }
    } else {
        if ($cav == 1) {
            $Template = "../Report_Template/1cav-1.xlsx";
        } else if ($cav == 2) {
            $Template = "../Report_Template/2cav-1.xlsx";
        } else if ($cav == 4) {
            $Template = "../Report_Template/4cav-1.xlsx";
        } else if ($cav == 5) {
            $Template = "../Report_Template/5cav-1.xlsx";
        } else if ($cav == 6) {
            $Template = "../Report_Template/6cav-1.xlsx";
        } else if ($cav == 8) {
            $Template = "../Report_Template/8cav-1.xlsx";
        } else {
            $Template = "../Report_Template/9cav-1.xlsx";
        }
    }

    //創建資料夾
    $path = "../../../../Report/Projector_System/XZ_MIM/Daily_Report/" . $Project_Name . "/" . $Furnace_No . "/";
    if (!file_exists($path)) {
        mkdir($path, 0777, true);
    }

    //定義報告檔名
    if ($Department == 1) {
        $Department_Name = "品保";
    } else if ($Department == 2) {
        $Department_Name = "製造";
    } else {
        $Machine_No = "";
    }

    $station_array = array("生胚" => "1", "燒結" => "2", "整形" => "3", "電鍍" => "4", "燒結+噴砂" => "5", "研磨" => "6", "噴砂" => "7", "CNC" => "8", "噴漆" => "9", "髮絲" => "10", "鈍化" => "11", "PVD" => "12");
    $Station_Name  = array_search($Station, $station_array);

    $Department_Name = iconv("utf-8", "big5", $Department_Name);
    $Station_Name    = iconv("utf-8", "big5", $Station_Name);

    if ($Spec_Status == 'ALL') {
        $Type          = iconv("utf-8", "big5", "全尺寸");
        $filename_xlsx = $Type . "_" . $Department_Name . "_" . $Mold_Number . "_" . $Furnace_No . "_" . $Part_Number_V . "_" . $Station_Name . "_" . date('Ymd', strtotime($End_Date)) . ".xlsx";
    } else {
        $filename_xlsx = $Department_Name . "_" . $Mold_Number . "_" . $Furnace_No . "_" . $Part_Number_V . "_" . $Station_Name . "_" . date('Ymd', strtotime($End_Date)) . ".xlsx";
    }

    //判別檔案是否存在
    $filepath    = $path . "/" . $filename_xlsx;
    $newfilepath = "../../../../Report/Projector_System/XZ_MIM/Daily_Report/" . $Project_Name . "/" . $filename_xlsx;

    if (file_exists($filepath)) {
        rename($filepath, $newfilepath); //將檔案移至新位子
        $load_report = PHPExcel_IOFactory::load($newfilepath); //開啟新位子報告
    } else {
        $load_report = PHPExcel_IOFactory::load($Template);
    }

    $tt = ceil(count($DataAll) / $num);

    for ($pp = 1; $pp <= $tt; $pp++) {
        $prev_value = "";
        if ($cav < 5) {
            if (($pp != 1) and ($pp % 2 == 1)) { //$pp % 2 == 1代表$pp為奇數
                $index++; //頁碼加1
            }

            if ($pp % 2 == 0) { //$pp % 2 == 0代表$pp為偶數
                if ($cav == 1) {$letter = 'J';} //$letter為數據放置報表的欄位位子
                if ($cav == 2) {$letter = 'K';} //$letter為數據放置報表的欄位位子
                if ($cav == 4) {$letter = 'M';} //$letter為數據放置報表的欄位位子
            } else { //$pp % 2 == 1代表$pp為奇數
                $letter = 'A'; //$letter為數據放置報表的欄位位子
            }

        } else {
            if ($pp > 1) {
                $index++;
            }

            $letter = 'A';
        }

        if ($pp != 1) {
            $arrayData = array_slice($DataAll, $num * ($pp - 1), $num);
        } elseif ($pp == 1) {
            $arrayData = array_slice($DataAll, 0, $num); //從$DataAll中取$num的個數數據
        }

        //"製造單位"使用"整形"量測最多可量8次
        if ($Station == 3 && $Department == 2) {
            $sheetname_array = array("量測1" => 1, "量測2" => 2, "量測3" => 3, "量測4" => 4, "量測5" => 5, "量測6" => 6, "量測7" => 7, "量測8" => 8);
        } else {
            $sheetname_array = array("量測1" => 1, "量測2" => 2, "量測3" => 3, "量測4" => 4);
        }
        $sheetname = array_search($measure_time, $sheetname_array); //依據量測票號次數定義sheetname
        $load_report->setActiveSheetIndexByName($sheetname . '-' . $index); //定義數據欲放置檔案中的頁面名稱
        $get_activesheet = $load_report->getActiveSheet(); //開啟此頁面

        //開始放置量測數據
        if ($arrayData) {
            $get_activesheet->fromArray($arrayData, null, $letter . $row);
        }

        //依據Station抓取$Machine_No
        if ($Station == 1) {
            $Machine_No = $Molding_M;
        } else if ($Station == 2) {
            $Machine_No = $Furnace_M;
        }

        $Part_Number = substr($Part_Number_V, 0, 14);
        $Cad_Rev     = substr($Part_Number_V, 15);
        $Mold_Number = $Mold_Number;

        //將產品表頭資訊放置報告中
        $get_activesheet->setCellValue('C2', $Part_Number);
        $get_activesheet->setCellValue('C3', $Project_Name);
        $i  = 0;
        $yy = count($Remark) + 5;

        if ($cav == 1) {
            $get_activesheet->setCellValue('G2', $Mold_Number);
            $get_activesheet->setCellValue('G3', $End_Date);
            $get_activesheet->setCellValue('M2', $Ticket);
            $get_activesheet->setCellValue('M3', $Machine_No);
            $get_activesheet->setCellValue('P2', $Cad_Rev);
            $get_activesheet->setCellValue('P3', $Material_Spec);
            $get_activesheet->setCellValue('N51', $Personal_ID);

            if ($pp % 2 == 1 && $Remark[$measure_time - 1] != "") {

                $get_activesheet->setCellValue('S6', $Remark[$measure_time - 1]);
            }

        } else if ($cav == 2) {
            $get_activesheet->setCellValue('G2', $Mold_Number);
            $get_activesheet->setCellValue('G3', $End_Date);
            $get_activesheet->setCellValue('O2', $Ticket);
            $get_activesheet->setCellValue('O3', $Machine_No);
            $get_activesheet->setCellValue('R2', $Cad_Rev);
            $get_activesheet->setCellValue('R3', $Material_Spec);
            $get_activesheet->setCellValue('O51', $Personal_ID);

            if ($pp % 2 == 1 && $Remark[$measure_time - 1] != "") {

                $get_activesheet->setCellValue('U6', $Remark[$measure_time - 1]);
            }

        } else if ($cav == 4) {
            $get_activesheet->setCellValue('H2', $Mold_Number);
            $get_activesheet->setCellValue('H3', $End_Date);
            $get_activesheet->setCellValue('O2', $Ticket);
            $get_activesheet->setCellValue('O3', $Machine_No);
            $get_activesheet->setCellValue('U2', $Cad_Rev);
            $get_activesheet->setCellValue('U3', $Material_Spec);
            $get_activesheet->setCellValue('Q51', $Personal_ID);

            if ($pp % 2 == 1 && $Remark[$measure_time - 1] != "") {

                $get_activesheet->setCellValue('Y6', $Remark[$measure_time - 1]);
            }

        } else if ($cav == 5) {
            $get_activesheet->setCellValue('G2', $Mold_Number);
            $get_activesheet->setCellValue('G3', $End_Date);
            $get_activesheet->setCellValue('J2', $Ticket);
            $get_activesheet->setCellValue('J3', $Machine_No);
            $get_activesheet->setCellValue('M2', $Cad_Rev);
            $get_activesheet->setCellValue('M3', $Material_Spec);
            $get_activesheet->setCellValue('L51', $Personal_ID);

            if ($pp % 2 == 1 && $Remark[$measure_time - 1] != "") {

                $get_activesheet->setCellValue('N6', $Remark[$measure_time - 1]);
            }

        }

         else if ($cav == 9) {
            $get_activesheet->setCellValue('G2', $Mold_Number);
            $get_activesheet->setCellValue('G3', $End_Date);
            $get_activesheet->setCellValue('J2', $Ticket);
            $get_activesheet->setCellValue('J3', $Machine_No);
            $get_activesheet->setCellValue('O2', $Cad_Rev);
            $get_activesheet->setCellValue('O3', $Material_Spec);
            $get_activesheet->setCellValue('L51', $Personal_ID);

            if ($pp % 2 == 1 && $Remark[$measure_time - 1] != "") {

                $get_activesheet->setCellValue('R6', $Remark[$measure_time - 1]);
            }
		 }

	
		else if ($cav == 6 or $cav == 8) {
            $get_activesheet->setCellValue('G2', $Mold_Number);
            $get_activesheet->setCellValue('G3', $End_Date);
            $get_activesheet->setCellValue('J2', $Ticket);
            $get_activesheet->setCellValue('J3', $Machine_No);
            $get_activesheet->setCellValue('N2', $Cad_Rev);
            $get_activesheet->setCellValue('N3', $Material_Spec);
            $get_activesheet->setCellValue('L51', $Personal_ID);
            if ($cav == 6) {

                if ($pp % 2 == 1 && $Remark[$measure_time - 1] != "") {

                    $get_activesheet->setCellValue('O6', $Remark[$measure_time - 1]);
                }

            } else {

                if ($pp % 2 == 1 && $Remark[$measure_time - 1] != "") {

                    $get_activesheet->setCellValue('Q6', $Remark[$measure_time - 1]);
                }

            }
        }

        //填寫外觀頁表頭資訊
        $load_report->setActiveSheetIndexByName('外觀頁');
        $get_activesheet = $load_report->getActiveSheet();
        $get_activesheet->setCellValue('B3', $Part_Number);
        $get_activesheet->setCellValue('B4', $Project_Name);
        $get_activesheet->setCellValue('F3', $Mold_Number);
        $get_activesheet->setCellValue('F4', $End_Date);
        $get_activesheet->setCellValue('J3', $Ticket);
        $get_activesheet->setCellValue('J4', $Machine_No);
        $get_activesheet->setCellValue('N3', $Cad_Rev);
        $get_activesheet->setCellValue('N4', $Material_Spec);

        //填寫製程巡迴檢查表
        $load_report->setActiveSheetIndexByName('製程巡迴檢查表');
        $get_activesheet = $load_report->getActiveSheet();
        $get_activesheet->setCellValue('E2', $Part_Number_V);
        $get_activesheet->setCellValue('H2', $Project_Name);
        $get_activesheet->setCellValue('C2', $End_Date);

        for ($y = 5; $y < $yy; $y++) {
            if ($Remark[$y - 5]) {
                $get_activesheet->setCellValue('H' . $y, $Remark[$y - 5]);
            }
        }

        $status_array = array("B" => "1", "C" => "2", "D" => "3", "E" => "4", "F" => "5", "G" => "6");
        $Status_P     = array_search($Status, $status_array); //依據$Status定義"V"放置位子

        if ($measure_time == 1) {
            $get_activesheet->setCellValue('A5', $Time);
            $get_activesheet->setCellValue($Status_P . '5', 'V');
        } else if ($measure_time == 2) {
            $get_activesheet->setCellValue('A6', $Time);
            $get_activesheet->setCellValue($Status_P . '6', 'V');
        } else if ($measure_time == 3) {
            $get_activesheet->setCellValue('A7', $Time);
            $get_activesheet->setCellValue($Status_P . '7', 'V');
        } else if ($measure_time == 4) {
            $get_activesheet->setCellValue('A8', $Time);
            $get_activesheet->setCellValue($Status_P . '8', 'V');
        } else if ($measure_time == 5) {
            $get_activesheet->setCellValue('A9', $Time);
            $get_activesheet->setCellValue($Status_P . '9', 'V');
        } else if ($measure_time == 6) {
            $get_activesheet->setCellValue('A10', $Time);
            $get_activesheet->setCellValue($Status_P . '10', 'V');
        } else if ($measure_time == 7) {
            $get_activesheet->setCellValue('A11', $Time);
            $get_activesheet->setCellValue($Status_P . '11', 'V');
        } else if ($measure_time == 8) {
            $get_activesheet->setCellValue('A12', $Time);
            $get_activesheet->setCellValue($Status_P . '12', 'V');
        }

        //儲存檔案
        $objWriter = PHPExcel_IOFactory::createWriter($load_report, 'Excel2007');
        $objWriter->setIncludeCharts(true);
        $objWriter->setPreCalculateFormulas(false);
        $objWriter->save($filepath);

    }

    if (file_exists($newfilepath)) {
        unlink($newfilepath); //删除新目录下的檔案
    }
} //while($ResultTicket = mysqli_fetch_assoc($QTicket) )
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>無標題文件</title>
</head>

<script>
alert ('量測數據已成功上傳&生成報表')
location.href="labtest14.php";
</script>
<body>
</body>
</html>
