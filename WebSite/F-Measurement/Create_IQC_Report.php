<?php
require_once '../../../../Public/Connections/projector_system_XZ_MIM_web.php';
require_once '../../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once '../../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';

ini_set("memory_limit", "-1");
ini_set("max_execution_time", '0');
//error_reporting(0);

//全形轉半形array
$fstr = array(
    "０", "１", "２", "３", "４", "５", "６", "７", "８", "９",
    "ａ", "ｂ", "ｃ", "ｄ", "ｅ", "ｆ", "ｇ", "ｈ", "ｉ", "ｊ",
    "ｋ", "ｌ", "ｍ", "ｎ", "ｏ", "ｐ", "ｑ", "ｒ", "ｓ", "ｔ",
    "ｕ", "ｖ", "ｗ", "ｘ", "ｙ", "ｚ",
    "Ａ", "Ｂ", "Ｃ", "Ｄ", "Ｅ", "Ｆ", "Ｇ", "Ｈ", "Ｉ", "Ｊ",
    "Ｋ", "Ｌ", "Ｍ", "Ｎ", "Ｏ", "Ｐ", "Ｑ", "Ｒ", "Ｓ", "Ｔ",
    "Ｕ", "Ｖ", "Ｗ", "Ｘ", "Ｙ", "Ｚ",
    "　",
);
$hstr = array(
    "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
    "a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
    "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
    "u", "v", "w", "x", "y", "z",
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
    "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
    "U", "V", "W", "X", "Y", "Z",
    " ",
);

//全形轉半形
$Project_Name0  = str_replace($fstr, $hstr, $_GET['Project_Name']);
$Part_Number_V0 = str_replace($fstr, $hstr, $_GET['Part_Number_V']);
$Mold_Number0   = str_replace($fstr, $hstr, $_GET['Mold_Number']);
@$Furnace_No0   = str_replace($fstr, $hstr, $_GET['Furnace_No']);

//強制轉大寫
$Project_Name  = strtoupper($Project_Name0);
$Part_Number_V = strtoupper($Part_Number_V0);
$Mold_Number   = strtoupper($Mold_Number0);
$Furnace_No1   = strtoupper($Furnace_No0);
$Furnace_No    = iconv("utf-8", "big5", $Furnace_No1);

$Department     = $_GET['Department'];
$Spec_Status    = $_GET['Spec_Status'];
$Furnace_M      = $_GET['Furnace_M'];
$Molding_M      = $_GET['Molding_M'];
$Status         = $_GET['Status'];
$Station        = $_GET['Station'];
$Machine_Num    = $_GET['Machine_Num'];
$Plating_Vendor = $_GET['Plating_Vendor'];
$Ticket_Number  = $_GET['Ticket_Number'];
$cav            = $_GET['Cav'];
$End_Date       = date("Y-m-d");

//利用array中"Order欄位"排序
function score_sort($a, $b)
{
    if ($a['0'] == $b['0']) {
        return 0;
    }
    return ($a['0'] > $b['0']) ? 1 : -1;
}

$t            = 0;
$Cavity_table = 'mim_measure_data_' . $cav . 'cav';

//依據每一個票號抓取數據
$QData = "SELECT * FROM " . $Cavity_table . " WHERE Ticket_Number = '" . $Ticket_Number . "' order by `Position`,`FAI_Number`,`Cavity_Number`";
echo $QData;
unset($DataAll);
mysqli_select_db($connect, $database);
$ResultData = mysqli_query($connect, $QData);
while ($Result = mysqli_fetch_assoc($ResultData)) {
    if ($Result['Personal_ID'] != '') {
        $Personal_ID = $Result['Personal_ID'];
    }

    if ($Result['Measure_Datetime'] == '0000-00-00 00:00:00') {
        $MeasureValue     = '';
        $Measure_Datetime = '';
        $Measure_Result   = '';
    } //if($Result['Measure_Datetime']=='0000-00-00 00:00:00')
    else {
        $MeasureValue     = $Result['Measure_Value'];
        $Measure_Datetime = $Result['Measure_Datetime'];
        $Measure_Result   = $Result['Measure_Result'];
    } //else
    if ($t == 0) //the beginning of a FAI
    {
        unset($Data_Array);
        unset($Data_Array1);
        unset($Data_Array2);
        $Data_Array         = array();
        $MeasureValue_Array = array();

        //將FAI號碼後面串Position
        if ($Result['Position'] == '') {
            $FAI_Number = $Result['FAI_Number'];
            $Order      = $Result['Order'];
        } else {
            $FAI_Number = $Result['FAI_Number'] . "_" . $Result['Position'];
            $Order      = $Result['Order'];
        }
        $Data_Array = array($Order, $FAI_Number, '', $Result['Nominal_Dim'], $Result['Lower_Dim'], $Result['Upper_Dim']);

    } //if($t==0)
    array_push($Data_Array, $MeasureValue); //將$MeasureValue加到$Data_Array之後
    if ($MeasureValue != "") {
        array_push($MeasureValue_Array, $MeasureValue); //將$MeasureValue加到$MeasureValue_Array之後
    }
    $t++;
    if ($t == ($cav)) {
        $last = 6 + $cav;
        $OK   = 0;
        $Null = 0;

        //計算Upper_Percentage & Lower_Percentage
        @$Max_MeasureValue = max($MeasureValue_Array);
        @$Min_MeasureValue = min($MeasureValue_Array);
        $Avg               = ($Data_Array[4] + $Data_Array[5]) / 2;

        if ($Max_MeasureValue == 0 & $Min_MeasureValue == 0) {
            $Upper_Percentage = '';
            $Lower_Percentage = '';
        } else {
            if ($Data_Array[4] == 0) {
                @$Upper_Percentage = $Max_MeasureValue * 100 / $Data_Array[5];
                @$Lower_Percentage = $Min_MeasureValue * 100 / $Data_Array[5];
            } else {
                @$Upper_Percentage = ($Max_MeasureValue - $Avg) * 100 / (($Data_Array[5] - $Data_Array[4]) / 2);
                @$Lower_Percentage = ($Min_MeasureValue - $Avg) * 100 / (($Data_Array[5] - $Data_Array[4]) / 2);
            }
        }

        for ($i = 6; $i < $last; $i++) {
            if ($Data_Array[$i] == '') {
                $Null++;
            } else if ($Data_Array[$i] <= $Data_Array[5] && $Data_Array[$i] >= $Data_Array[4]) {
                $OK++;
            }
        }

        //小於8穴時Measure Data加空值
        if ($cav < 8) {
            $add = 8 - $cav;
            for ($j = 0; $j < $add; $j++) {array_push($Data_Array, "");}
        }

        //將量測結果& Upper_Percentage & Lower_Percentage放置$Data_Array
        $add_OKNull = $OK + $Null;
        if ($Null == $cav) {
            array_push($Data_Array, "", $Upper_Percentage, $Lower_Percentage);
        } else {
            if ($add_OKNull == $cav) {
                array_push($Data_Array, "OK", $Upper_Percentage, $Lower_Percentage);
            } else {
                array_push($Data_Array, "NG", $Upper_Percentage, $Lower_Percentage);
            }
        }
        //print_r($Data_Array);
        $DataAll[] = $Data_Array;
        $t         = 0;
    } //if($t==($cav))
} //while($Result = mysqli_fetch_assoc($ResultData))

//將2維array利用FAI_Number欄位做排序
usort($DataAll, 'score_sort');

//去除array中"Order"欄位
if ($cav <= 8) {$num = 16;} else { $num = 17;}
//$num = 8 + $cav;
foreach ($DataAll as $k => $New_DataAll) {
    $DataAll[$k] = array_slice($New_DataAll, 1, $num);
}

$index = 1; //起始頁碼
$row   = 17; //報表量測數據放置起始位子
$num   = 13; //報表每一頁能放置量測數據的數量

//報告樣板依據$Cav切換
if ($cav == 9) {
    $Template = "../Report_Template/IQC_9cav.xlsx";
} else {
    $Template = "../Report_Template/IQC_8cav.xlsx";
}

//創建資料夾
$path = "../../../../Report/Projector_System/XZ_MIM/IQC_Report/" . $Project_Name . "/" . $Furnace_No . "/";

if (!file_exists($path)) {
    mkdir($path, 0777, true);
}

//定義報告檔名
if ($Department == 1) {
    $Department_Name = "品保";
} else if ($Department == 2) {
    $Department_Name = "製造";
}

$station_array = array("生胚" => "1", "燒結" => "2", "整形" => "3", "電鍍" => "4", "燒結噴砂" => "5", "研磨" => "6", "噴砂" => "7", "CNC" => "8", "噴漆" => "9", "髮絲" => "10", "鈍化" => "11", "PVD" => "12");
$Station_Name  = array_search($Station, $station_array);

$Department_Name = iconv("utf-8", "big5", $Department_Name);
$Station_Name    = iconv("utf-8", "big5", $Station_Name);

$Type          = iconv("utf-8", "big5", "出貨報告");
$filename_xlsx = $Type . "_" . $Department_Name . "_" . $Mold_Number . "_" . $Furnace_No . "_" . $Part_Number_V . "_" . $Station_Name . "_" . date('Ymd', strtotime($End_Date)) . ".xlsx";
$filepath      = $path . "/" . $filename_xlsx;
$load_report   = PHPExcel_IOFactory::load($Template);

$tt = ceil(count($DataAll) / $num);

for ($pp = 1; $pp <= $tt; $pp++) {

    if ($pp > 1) {
        $index++; //頁碼加1
    }
    $letter = 'B'; //$letter為數據放置報表的欄位位子

    if ($pp != 1) {
        $arrayData = array_slice($DataAll, $num * ($pp - 1), $num);
    } elseif ($pp == 1) {
        $arrayData = array_slice($DataAll, 0, $num); //從$DataAll中取$num的個數數據
    }

    $load_report->setActiveSheetIndexByName('量測-' . $index); //定義數據欲放置檔案中的頁面名稱
    $get_activesheet = $load_report->getActiveSheet(); //開啟此頁面

    //開始放置量測數據
    if ($arrayData) {
        $get_activesheet->fromArray($arrayData, null, $letter . $row);
    }

    //依據Station抓取$Machine_No
    if ($Station == 1) {
        $Machine_No = $Molding_M;
    } else if ($Station == 2) {
        $Machine_No = $Furnace_M;
    } else {
        $Machine_No = "";
    }

    //將產品表頭資訊放置報告中
    $Part_Number = substr($Part_Number_V, 0, 14);
    $Cad_Rev     = substr($Part_Number_V, 15);
    $Mold_Number = $Mold_Number;

    $get_activesheet->setCellValue('E10', $Part_Number);
    $get_activesheet->setCellValue('E8', $Project_Name);
    $get_activesheet->setCellValue('C6', $End_Date);
    $get_activesheet->setCellValue('I10', $Cad_Rev);
    $get_activesheet->setCellValue('M36', $Personal_ID);
    $get_activesheet->setCellValue('L8', $Plating_Vendor);

    //儲存檔案
    $objWriter = PHPExcel_IOFactory::createWriter($load_report, 'Excel2007');
    $objWriter->setIncludeCharts(true);
    $objWriter->setPreCalculateFormulas(false);
    $objWriter->save($filepath);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>無標題文件</title>
</head>

<script>
//alert ('量測數據已成功上傳&生成IQC報表')
location.href="labtest14.php";
</script>
<body>
</body>
</html>
