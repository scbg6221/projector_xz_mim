<?php
session_start();
error_reporting(0);
include '../../../../Public/MainWebUI/User_Count.php';
include '../../../../Public/MainWebUI/Login_Control.php';
require_once('../../../../Public/Connections/projector_system_XZ_MIM_web.php'); 
require_once('../../../../Public/library/PHPExcel/php-excel-reader-2.21/simplexlsx.class.php');

//ini_set("memory_limit", "-1"); 
ini_set("max_execution_time", '0');
  if(!empty($_POST['Part_Number_VF']) && !empty($_POST['FAI_NumberF']) && !empty($_POST['Cpk_TargetF']) && !empty($_POST['Start_DateF']) && !empty($_POST['End_DateF'])){

$Factory=$_POST['FactoryF'];
$Department=$_POST['DepartmentF'];
$User=$_POST['UserF'];
$Station=$_POST['Station'];
$Mold_Number=$_POST['Mold_NumberF'];
$Part_Name=$_POST['Part_NameF'];
$Part_Number_V=$_POST['Part_Number_VF'];
$Sampling_Rate=$_POST['Sampling_RateF'];
$FAI_Number=$_POST['FAI_NumberF']; 
$Start_Date=$_POST['Start_DateF'];
$End_Date=$_POST['End_DateF']; 
$Cpk_Target=$_POST['Cpk_TargetF'];
$Cav=$_POST['Cav'];
$submit=$_POST['submitA'];
$FAI=$FAI_Number.'1';

$FAI1 = array();
mysqli_select_db($connect,$database );
for($i=0;$i<5;$i++)
$FAI1[$i]=$FAI_Number;

$FAI_Nums = 5;
$FAI_Nums1 = 5;
$_SESSION['FAI_Nums1']=$FAI_Nums1;

///定義Tital1,2,3,4,5///
if ($FAI_Nums=='5'){
	
	$Dim=array('Date','Time','FAI-1','FAI-2','FAI-3','FAI-4','FAI-5','Sum','X_Bar','R');	
    $t=1;
	foreach($Dim as $v)
{   ${'Tital'.$t}=array($v);	
    $t++;
}

$AllMeasureData=$Nominal_Dim=$Upper_Dim=$Lower_Dim=$Total_Samples=$Group_Nums=$Date1=$Time1=$Date=$Time=$sum1=$avg1=$R1=$sum=$avg=$R=$CLxx=$UCLxx=$LCLxx=$CLrr=$UCLrr=$LCLrr=array();

for($t=1;$t<4;$t++)
${'Data'.$t}=array();


for($t=1;$t<4;$t++)
${'FAI'.$t}=array();

mysqli_select_db($connect,$database );
$Cav_Table_Q=
"SELECT
        Cavity,
		Ticket_Number						
    FROM mim_main_table WHERE  Part_Number_V= '".$Part_Number_V."' ";	
	
$Cav_Table_R=mysqli_query($connect,$Cav_Table_Q);
$R=mysqli_fetch_array($Cav_Table_R);
$cav_table="mim_measure_data_".$R['Cavity']."cav";

if($Department)
$Deptt="AND mim_main_table.Department='$Department'";
if($Station)
$Stationn="AND mim_main_table.Station='$Station'";


$Select_AllData="SELECT
	    mim_main_table.Start_Date,
		mim_main_table.Mold_Number,
		mim_main_table.Ticket_Number,
		mim_main_table.Cavity,	
		".$cav_table.".Ticket_Number,
		".$cav_table.".FAI_Number,
	    ".$cav_table.".Measure_Value,		
		".$cav_table.".Nominal_Dim,
		".$cav_table.".Upper_Dim,
		".$cav_table.".Lower_Dim,
		".$cav_table.".Cavity_Number,
		(SELECT COUNT(*) FROM mim_main_table,".$cav_table." WHERE   mim_main_table.Part_Number_V= '".$Part_Number_V."' AND mim_main_table.Mold_Number='".$Mold_Number."' AND ".$cav_table.".FAI_Number = '".$FAI_Number."' AND ".$cav_table.".Cavity_Number = '".$Cav."' ".$Deptt."".$Stationn." AND mim_main_table.End_Date between '".$Start_Date."' AND '".$End_Date."' AND ".$cav_table.".Ticket_Number=mim_main_table.Ticket_Number) AS Total_Samples 		
    FROM mim_main_table, ".$cav_table." WHERE  mim_main_table.Part_Number_V= '".$Part_Number_V."' AND mim_main_table.Mold_Number='".$Mold_Number."' AND ".$cav_table.".FAI_Number = '".$FAI_Number."' AND ".$cav_table.".Cavity_Number = '".$Cav."' ".$Deptt."".$Stationn." AND mim_main_table.End_Date between '".$Start_Date."' AND '".$End_Date."' AND ".$cav_table.".Ticket_Number=mim_main_table.Ticket_Number ORDER BY Start_Date ASC,Start_Time ASC";	
	//echo $Select_AllData;
	
$query_AllData = mysqli_query($connect, $Select_AllData) ;
$j=0;
while  ($AllData=mysqli_fetch_array($query_AllData))  {
$AllMeasureData[$j]=$AllData["Measure_Value"];	
$Nominal_Dim[$j] = $AllData['Nominal_Dim']; 
$Upper_Dim[$j] = $AllData['Upper_Dim']; 
$Lower_Dim[$j] = $AllData['Lower_Dim'];
$Total_Samples[$j] = $AllData['Total_Samples']; 
$j++;
}
 
$Upper_Dim=$Upper_Dim[0];
$Lower_Dim=$Lower_Dim[0];
$Total_Samples=$Total_Samples[0];
$Group_Nums = floor($Total_Samples/$FAI_Nums);

$_SESSION['Nominal_Dim']=$Nominal_Dim[0];
$_SESSION['Upper_Dim']=$Upper_Dim[0];
$_SESSION['Lower_Dim']=$Lower_Dim[0];
$_SESSION['Total_Samples']=$Total_Samples[0];
$_SESSION['Group_Nums']=$Group_Nums;

$Select_DateTime = "SELECT mim_main_table.Start_Date, mim_main_table.Start_Time, ".$cav_table.".FAI_Number FROM mim_main_table, ".$cav_table." WHERE mim_main_table.Part_Number_V = '".$Part_Number_V."' AND ".$cav_table.".FAI_Number = '".$FAI_Number."' AND ".$cav_table.".Cavity_Number = '".$Cav."'".$Deptt."".$Stationn." AND  mim_main_table.End_Date between '".$Start_Date."' AND '".$End_Date."' AND ".$cav_table.".Ticket_Number=mim_main_table.Ticket_Number  ORDER BY Start_Date ASC , Start_Time ASC" ;
$query_DateTime = mysqli_query($connect,$Select_DateTime );

$k=0;
$kk=0;
while  ($DateTime=mysqli_fetch_array($query_DateTime)) 
{
if($kk==0 || $kk%5==0)
{	
$Date1[$k]=$DateTime["Start_Date"];
$Time1[$k]=$DateTime["Start_Time"];
$k++; 
}
$kk++;

}
//print_r( $Total_Samples);
///Data數據分類並存入array///

/*
for($i=0;$i<5;$i++)
{
	${'a'.$i+1}=$i;
while(${'a'.$i+1}<$Total_Samples)
{   echo ${'a'.$i+1}."<BR>";
	${'Data'.$i+1}[$i]=$AllMeasureData[${'a'.$i+1}];
	${'a'.$i+1}+=5;
	}
}*/
//print_r($Data1);

$a1=0;
while($a1<$Total_Samples){
$Data1[$a1]=$AllMeasureData[$a1];
$a1=$a1+5; 
}

$a2=1;
while($a2<$Total_Samples){
$Data2[$a2]=$AllMeasureData[$a2];
$a2=$a2+5; 
}

$a3=2;
while($a3<$Total_Samples){
$Data3[$a3]=$AllMeasureData[$a3];
$a3=$a3+5; 
}

$a4=3;
while($a4<$Total_Samples){
$Data4[$a4]=$AllMeasureData[$a4];
$a4=$a4+5; 
}

$a5=4;
while($a5<$Total_Samples){
$Data5[$a5]=$AllMeasureData[$a5];
$a5=$a5+5; 
}

$a6=1;
while($a6<=$Group_Nums){
$Group1[$a6]=$a6;
$a6++; 
}

for ($y=1;$y<=$Group_Nums;$y++){
${'groupdata'.$y} = array();
}

$b1=0;
for($b2=0;$b2<$Group_Nums;$b2++) {
	for($b3=$b1;$b3<$FAI_Nums;$b3++) {
${'groupdata'.$b2}[$b3]=$AllMeasureData[$b3];
} 

$b1=$b1+5; 
$FAI_Nums=$FAI_Nums+5; 
}//print_r($groupdata1);
$_SESSION['Group1']=$Group1;
$_SESSION['Date2']=$Date1;
$_SESSION['Time1']=$Time1;
$_SESSION['Data1']=$Data1;
$_SESSION['Data2']=$Data2;
$_SESSION['Data3']=$Data3;
$_SESSION['Data4']=$Data4;
$_SESSION['Data5']=$Data5;


for ($b4=0;$b4<$Group_Nums;$b4++){	
${'groupsum'.$b4}=round(array_sum(${'groupdata'.$b4}),4);
${'groupmax'.$b4}=max(${'groupdata'.$b4});
${'groupmin'.$b4}=min(${'groupdata'.$b4});
// $R  ////
${'groupR'.$b4}=round((${'groupmax'.$b4})-(${'groupmin'.$b4}),4);

/// $avg  ///
${'groupavg'.$b4}=round(((${'groupsum'.$b4})/$FAI_Nums1),4);
$sum1[]=${'groupsum'.$b4};
$R1[]=${'groupR'.$b4};
$avg1[]=${'groupavg'.$b4};
}


$_SESSION['sum1']=$sum1;
$_SESSION['R1']=$R1;
$_SESSION['avg1']=$avg1;

$A2=0.577;
$d2=2.326;
$D3=0.000;
$D4=2.114;

///運算Cpk,Cp...各參數///
$avg_sum=array_sum($avg1);
$CLx1=$avg_sum/$Group_Nums;

$CLx=round($CLx1, 3);
$_SESSION['CLx']=$CLx; 
  
$R_sum=array_sum($R1);
$CLr1=$R_sum/$Group_Nums;
$CLr=round($CLr1, 3); 
$_SESSION['CLr']=$CLr;

$UCLx1=$CLx1+$A2*$CLr1;
$UCLx=round($UCLx1, 3); 
$LCLx1=$CLx1-$A2*$CLr1;
$LCLx=round($LCLx1, 3);
$_SESSION['UCLx']=$UCLx;
$_SESSION['LCLx']=$LCLx; 
  
$UCLr1=$D4*$CLr1;
$UCLr=round($UCLr1, 3); 
$LCLr1=$D3*$CLr1;
$LCLr=round($LCLr1, 3);
$_SESSION['UCLr']=$UCLr;
$_SESSION['LCLr']=$LCLr; 
  
$C1=$CLr1/$d2;
$C=round($C1, 4); 
  //echo $C1;
$USL=$Upper_Dim;
$LSL=$Lower_Dim;


{$Cp1=($USL-$LSL)/(6*$C1);}
$Cp=round($Cp1,2);
$_SESSION['Cp']=$Cp; 

$CpU1=($USL-$CLx1)/(3*$C1);
$CpU=round($CpU1,2);
$_SESSION['CpU']=$CpU; 
  
$CpL1=($CLx1-$LSL)/(3*$C1);
$CpL=round($CpL1,2);
$_SESSION['CpL']=$CpL;

$Ca1=abs((($USL+$LSL)/2)-$CLx1);  
$Ca2=$Ca1/(($USL-$LSL)/2);
$Ca3=$Ca2*100; 
$Ca=round($Ca3,2);
$_SESSION['Ca']=$Ca;
  
$Cpk1=(1-$Ca2)*$Cp1;
$Cpk=round($Cpk1,2);
$_SESSION['Cpk']=$Cpk;

$j6=0;
while($j6<$Group_Nums) {
$CLrr[$j6]=$CLr;
$UCLrr[$j6]=$UCLr;
$LCLrr[$j6]=$LCLr;
$CLxx[$j6]=$CLx;
$UCLxx[$j6]=$UCLx;
$LCLxx[$j6]=$LCLx;
$j6++; 
} 

$_SESSION['CLxx']=$CLxx;
$_SESSION['UCLxx']=$UCLxx;
$_SESSION['LCLxx']=$LCLxx;
$_SESSION['CLrr']=$CLrr;
$_SESSION['UCLrr']=$UCLrr;
$_SESSION['LCLrr']=$LCLrr;

///加入抬頭///
//print_r($Tital1);
$Date=array_merge($Tital1,$Date1);
$Time=array_merge($Tital2,$Time1);
$FAI1=array_merge($Tital3,$Data1);
$FAI2=array_merge($Tital4,$Data2);
$FAI3=array_merge($Tital5,$Data3);
$FAI4=array_merge($Tital6,$Data4);
$FAI5=array_merge($Tital7,$Data5);
$sum=array_merge($Tital8,$sum1);
$avg=array_merge($Tital9,$avg1);
$R=array_merge($Tital10,$R1); 
}

else {
	echo "<script>alert('Please confirm this FAI is CPK');</script>";
}

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>無標題文件</title>
<style>

#table-2 {
	border: 1px solid #e3e3e3;
	background-color: #f2f2f2;
	border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	margin-left:0%;   
	margin-top:2%;	
}
#table-2 td {
	padding: 5px;
	width: 20%;
	color: #333;
}
#table-2 thead {
	font-family:Arial, Helvetica, sans-serif;
	padding: .2em 0 .2em .5em;
	text-align: left;
	color: #4B4B4B;
	background-color: #C8C8C8;
	background-image: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#e3e3e3), color-stop(.6,#B3B3B3));
	background-image: -moz-linear-gradient(top, #D6D6D6, #B0B0B0, #B3B3B3 90%);
	border-bottom: solid 1px #999;
	width:auto;

}
#table-2 th {
	width:auto;
    padding: 5px;
    color: #333;
	font-family:Arial, Helvetica, sans-serif;
	font-size: 14px;
	line-height: 20px;
	font-style: normal;
	font-weight: normal;
	text-align: center;
	text-shadow: white 1px 1px 1px;
	width:auto;
	column-span:6;
}
#table-2 td {
	line-height: 30px;
	font-family:Arial, Helvetica, sans-serif;
	font-size: 12px;
	width:auto;
	border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;
}
#table-2 td:hover {
	background-color: #fff;
}

L{
	font-family:"Arial Black", Gadget, sans-serif;
	color:#009EA0;
	font-size:20px;
	}

M{
	font-family:"Arial Black", Gadget, sans-serif;
	color:#000000;
	font-size:14px;
	}
	
N{
	font-family:"Arial Black", Gadget, sans-serif;
	color:#0300FA;
	font-size:14px;
	}
	
</style>
</head>

<body>
<table width="800" border="2"><L>Information</L>
  <tr>
    <td width="133" align="center" valign="middle" nowrap="nowrap"><M>Factory</M></td>
    <td width="133" align="center" valign="middle" nowrap="nowrap"><M>Department</M></td>
    <td width="178" align="center" valign="middle" nowrap="nowrap"><M>User</M></td>
    <td align="center" valign="middle" nowrap="nowrap"><M>Mold Number</M></td>
    <td align="center" valign="middle" nowrap="nowrap"><M>Part Name</M></td>
  </tr>

  <tr>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $Factory; ?></td>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $Department; ?></td>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $User; ?></td>
    <td width="183" align="center" valign="middle" nowrap="nowrap"><?php echo $Mold_Number; ?></td>
    <td width="142" align="center" valign="middle" nowrap="nowrap"><?php echo $Part_Name; ?></td>
  </tr>

  <tr>
    <td align="center" valign="middle" nowrap="nowrap"><M>Part Number</M></td>
    <td width="97" align="center" valign="middle" nowrap="nowrap"><M>Cpk Target</M></td>
    <td align="center" valign="middle" nowrap="nowrap"><M>Group Numbers</M></td>
    <td align="center" valign="middle" nowrap="nowrap"><M>Sampling Sizes (Pcs</M>)</td>
    <td align="center" valign="middle" nowrap="nowrap"><M>Sampling Rates (Hrs)</M></td>

  <tr>
    <td width="149" align="center" valign="middle" nowrap="nowrap"><?php echo $Part_Number_V; ?></td>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $Cpk_Target; ?></td> 
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $Group_Nums; ?></td>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $FAI_Nums1; ?></td>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $Sampling_Rate; ?></td>
  </tr>
  <tr>
    <td align="center" valign="middle" nowrap="nowrap"><M>Start Date</M></td>
    <td align="center" valign="middle" nowrap="nowrap"><M>End Date</M></td>
    <td width="142" align="center" valign="middle" nowrap="nowrap"><M>Dimension(mm)</M></td>
    <td align="center" valign="middle" nowrap="nowrap"><M>USL</M></td>
    <td align="center" valign="middle" nowrap="nowrap"><M>LSL</M></td>
  </tr>
  <tr>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $Start_Date; ?></td>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $End_Date; ?></td> 
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $Nominal_Dim; ?></td>  
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $Upper_Dim; ?></td>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $Lower_Dim; ?></td>
  </tr>
  
<table width="750" border="2"><L>Result</L>
  <tr>
    <td width="50" align="center" valign="middle"><M>CLx</M></td>
    <td width="50" align="center" valign="middle"><M>UCLx</M></td>
    <td width="50" align="center" valign="middle"><M>LCLx</M></td>
    <td width="50" align="center" valign="middle"><M>CLr</M></td>
    <td width="50" align="center" valign="middle"><M>UCLr</M></td>
    <td width="50" align="center" valign="middle"><M>LCLr</M></td>
    <td width="50" align="center" valign="middle"><M>CpU</M></td>
    <td width="50" align="center" valign="middle"><M>CpL</M></td>
    <td width="50" align="center" valign="middle"><M>Cp</M></td>
    <td width="50" align="center" valign="middle"><M>Ca</M></td>
    <td width="50" align="center" valign="middle"><M>Cpk</M></td>
    <td colspan='2' width="200" align="center" valign="middle"><M>Graph Plot</M></td>

  </tr>
  <tr>
    <td width="50" align="center" valign="middle"><?php echo $CLx; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $UCLx; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $LCLx; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $CLr; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $UCLr; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $LCLr; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $CpU; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $CpL; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $Cp; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $Ca.'%'; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $Cpk; ?></td>
    <td width="100" align="center" valign="middle">
<?php
echo "<form name='1' id='1' method='post' action='X_Bar Chart-3.php' target='_blank'>";
$string1 = serialize($R1);
$string2 = serialize($CLrr);
$string3 = serialize($UCLrr);
$string4 = serialize($LCLrr);

echo "<input type='hidden' name=string1  value=".$string1.">";
echo "<input type='hidden' name=string2  value=".$string2.">";
echo "<input type='hidden' name=string3  value=".$string3.">";
echo "<input type='hidden' name=string4  value=".$string4.">";
echo "<input type='submit' name='submit1' id='submit1' value='Range(R Chart)'>";
echo "</form>";
?></td>
    <td width="100" align="center" valign="middle">
<?php
echo "<form name='2' id='2' method='post' action='X_Bar Chart-4.php' target='_blank'>";
$string5 = serialize($avg1);
$string6 = serialize($CLxx);
$string7 = serialize($UCLxx);
$string8 = serialize($LCLxx);

echo "<input type='hidden' name=string5  value=".$string5.">";
echo "<input type='hidden' name=string6  value=".$string6.">";
echo "<input type='hidden' name=string7  value=".$string7.">";
echo "<input type='hidden' name=string8  value=".$string8.">";
echo "<input type='submit' name='submit2' id='submit2' value='Average(X_Bar Chart)'>";
echo "</form>";
?></td>
  </tr>
</table>

<table id="table-2" name="table-2" method="post">
  <thead>
<th>Group NO.</th>

<?php
$c=1;

while($c<=$Group_Nums) {
echo "<th>".$c."</th>";
$c++; 
} 

?>

</thead>
<tbody>

<?php
//print_r($Date);
//echo $Group_Nums;
  for ($d=0; $d<=$Group_Nums; $d++){
  echo "<td align=center>" . $Date[$d] . "</td>";
}

  echo "</tr>";
  for ($d=0; $d<=$Group_Nums; $d++){
  echo "<td align=center>" . $Time[$d] . "</td>";
}

  echo "</tr>";
  for ($d=0; $d<=$Group_Nums; $d++){
  echo "<td align=center>" . $FAI1[$d] . "</td>";
}

  echo "</tr>";
  for ($d=0; $d<=$Group_Nums; $d++){
  echo "<td align=center>" . $FAI2[$d] . "</td>";
}

  echo "</tr>";
  for ($d=0; $d<=$Group_Nums; $d++){
  echo "<td align=center>" . $FAI3[$d] . "</td>";
}

  echo "</tr>";
  for ($d=0; $d<=$Group_Nums; $d++){
  echo "<td align=center>" . $FAI4[$d] . "</td>";
}

  echo "</tr>";
  for ($d=0; $d<=$Group_Nums; $d++){
  echo "<td align=center>" . $FAI5[$d] . "</td>";
}

  echo "</tr>";
  for ($d=0; $d<=$Group_Nums; $d++){
  echo "<td align=center>" . $sum[$d] . "</td>";
}

  echo "</tr>";
  for ($d=0; $d<=$Group_Nums; $d++){
  echo "<td align=center>" . $avg[$d] . "</td>";
}

  echo "</tr>";
  for ($d=0; $d<=$Group_Nums; $d++){
  echo "<td align=center>" . $R[$d] . "</td>";
}
  }
  else{header('Location: X_Bar Chart-6.php');}
?>

</tbody>
</table>


</body>
</html>