<?php

session_start();
require_once('../../../../Public/Connections/projector_system_XZ_MIM_web.php'); 
require_once ('../../../../Public/library/jpgraph/jpgraph-3.5.0b1/src/jpgraph.php');
require_once ('../../../../Public/library/jpgraph/jpgraph-3.5.0b1/src/jpgraph_line.php');
require_once ('../../../../Public/library/jpgraph/jpgraph-3.5.0b1/src/jpgraph_scatter.php');
$string1=trim($_POST['string1']);
$R = unserialize(( $string1 ) );
$string2=trim($_POST['string2']);
$CLr1 = unserialize(( $string2 ) );
$string3=trim($_POST['string3']);
$UCLr1 = unserialize(( $string3 ) );
$string4=trim($_POST['string4']);
$LCLr1 = unserialize(( $string4 ) );


// Setup the graph
$graph = new Graph(1200,400,"auto");
$graph->SetScale("textlin");

$theme_class= new UniversalTheme;
$graph->SetTheme($theme_class);
//$graph->img->SetImgFormat("png");
$graph->xaxis->title->Set("Gropu No.");
$graph->yaxis->title->Set("R");
$graph->xaxis->title->SetPos("max");
$graph->yaxis->title->SetPos("max");
$graph->yaxis->title->SetFont(FF_ARIAL,FS_BOLD,12);
$graph->xaxis->title->SetFont(FF_ARIAL,FS_BOLD,12);

$graph->title->Set('RANGE(R CHART)');
$graph->title->SetFont(FF_ARIAL,FS_BOLD,16);
$graph->SetBox(false);

$graph->yaxis->HideZeroLabel();
$graph->yaxis->HideLine(false);
$graph->yaxis->HideTicks(false,false);

$graph->xaxis->SetTickLabels(array('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25'));
$graph->xaxis->SetFont(FF_ARIAL,FS_NORMAL,8);
$graph->yaxis->SetFont(FF_ARIAL,FS_NORMAL,8);
$graph->xaxis->SetPos("min");
$graph->yaxis->SetPos("min");
$graph->ygrid->SetFill(false);

$p1 = new LinePlot($R);
$graph->Add($p1);
$p1->SetColor("#000000");
$p1->SetLegend('R');
$p1->mark->SetType(MARK_FILLEDCIRCLE,'',1.0);
$p1->mark->SetColor('#000000');
$p1->mark->SetFillColor('#000000');
$p1->SetCenter();

$graph->legend->SetFrameWeight(1);
$graph->legend->SetColor('#4E4E4E','#00A78A');
$graph->legend->SetMarkAbsSize(8);

$lineplot1 = new LinePlot($UCLr1,"red",2);
$graph->Add($lineplot1);
$lineplot1->SetColor("#F50000");
$lineplot1->SetLegend("UCLR");
//$lineplot1->SetLineStyle('dashed');

$lineplot2 = new LinePlot($CLr1);
$graph->Add($lineplot2);
$lineplot2->SetColor("#1C19FF");
$lineplot2->SetLegend("CLR");

$lineplot3 = new LinePlot($LCLr1);
$graph->Add($lineplot3);
$lineplot3->SetColor("#F50000");
$lineplot3->SetLegend("LCLR");

$graph->legend->SetColor('#000000');
$graph->legend->SetMarkAbsSize(8);
$graph->legend->SetLayout('1');
$graph->Stroke();
/*
mysqli_free_result($result,$Project_Name,$Mold_Number,$Start_Date,$End_Date);// 釋放變數
$mysqli->close(); */  //將mysql連線關閉*/
?>
