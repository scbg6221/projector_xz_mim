<?php

session_start();
error_reporting(0);
include '../../../../Public/MainWebUI/User_Count.php';
include '../../../../Public/MainWebUI/Login_Control.php';
require_once('../../../../Public/Connections/projector_system_XZ_MIM_web.php'); 


mysqli_select_db($connect, $database);
?>

<!DOCTYPE HTML>
<html >
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<script type="text/javascript" src="../../../../Public/library/JQuery/jquery-1.11.3/jquery-1.11.3.js"></script>
<script type="text/javascript" src="../../../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script type="text/javascript" src="../../../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="../../../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.css"/>
<link rel="stylesheet" type="text/css" href="../CSS/ALL_CSS.css?id='1234'">
<script>
$(function() {
  $( "#Start_DateF").datepicker({
  dateFormat:"yy-mm-dd",
  yearRange:"2014:2033",
  //defaultDate:-10,
  //showOn: "button",
 /// buttonImage: "images/calendar-blue.gif",
 // buttonImageOnly: true
})
  $( "#End_DateF").datepicker({
  dateFormat:"yy-mm-dd",
  yearRange:"2014:2033",
  //defaultDate:-10,
  //showOn: "button",
  buttonImage: "images/calendar-blue.gif",
  buttonImageOnly: true
})
})


</script>
<script type="text/javascript">


function Export()
{
    document['form1'].action = "X_Bar Chart-5.php";
    document['form1'].target = 'Index_Bottom-2';
	document['form1'].submit();
}
function submitA()
{
document['form1'].action  ="X_Bar Chart-2.php";
document['form1'].target = "Index_Bottom-2"
document['form1'].submit();
}
</script>



</head>

<body>
<form id="form1" name="form1" method="post"  >


<table width="50" style="width:100%">
<tr>
<th >  <div align="left">Factory:</div></th>
<th ><div align="left">
  <input type="text" name="FactoryF" id="FactoryF" class="XBar-2" value="<?php echo $_POST['FactoryF'] ?>" >
</div></th>
<th><div align="left">Dept.:</div></th>
<th><div align="left">
  <input type="text" name="DepartmentF" id="DepartmentF" class="XBar-3" value="<?php echo $_POST['DepartmentF'] ?>" >
</div></th>
<th><div align="left">User: </div></th>
<th><div align="left">
  <input type="text" name="UserF" id="UserF" class="XBar-3" value="<?php echo $_POST['UserF'] ?>"  >
</div></th>
<th><div align="left">Station:</div></th>
<th><div align="left">
  <input type="text"  name="Station"   id="Station" class="XBar-2" value="<?php echo $_POST['Station'] ?>" >
</div></th>
<th><div align="left">Part_Name:</div></th>
<th><div align="left">
  <input type="text"  name="Part_NameF"   id="Part_NameF" class="XBar-1" value="<?php echo $_POST['Part_NameF'] ?>" >
</div></th>
<th><div align="left"></div></th>
<th>&nbsp;</th>
</tr>
<tr>
<th><div align="left">Mold_No.:</div></th>
<th><div align="left">
  <input type="text" name="Mold_NumberF" id="Mold_NumberF" class="XBar-3" value="<?php echo $_POST['Mold_NumberF'] ?>">
</div></th>
<th><div align="left">Part_No.(*):</div></th>
<th><div align="left">
  <input type="text"  name="Part_Number_VF"   id="Part_Number_VF" class="XBar-1" value="<?php echo $_POST['Part_Number_VF'] ?>">
</div></th>
<th><div align="left">Cav(*):</div></th>
<th><div align="left">
  <input type="text"  name="Cav"   id="Cav" class="XBar-3" value="<?php echo $_POST['Cav'] ?>" >
</div></th>
<th><div align="left">FAI_No.(*):</div></th>
<th><div align="left">
  <input type="text"  name="FAI_NumberF"   id="FAI_NumberF" class="XBar-2" value="<?php echo $_POST['FAI_NumberF'] ?>" >
</div></th>
<th><div align="left">Cpk_Target(*): </div></th>
<th><div align="left">
  <input type="text" name="Cpk_TargetF" id="Cpk_TargetF" class="XBar-2" value="<?php echo $_POST['Cpk_TargetF'] ?>"  >
</div></th>
<th><div align="left"></div></th>
<th>&nbsp;</th>
</tr>
<tr>
<th rowspan="2"><div align="left">Sampling_Rate:</div></th>
<th rowspan="2"><div align="left">
  <input type="text"  name="Sampling_RateF"   id="Sampling_RateF" class="XBar-2" value="<?php echo $_POST['Sampling_RateF'] ?>" >
</div></th>
<th rowspan="2"><div align="left">From(*):</div></th>
<th rowspan="2"><div align="left">
  <input type="text"  name="Start_DateF"   id="Start_DateF" class="XBar-3" value="<?php echo $_POST['Start_DateF'] ?>"   >
</div></th>
<th rowspan="2"><div align="left">To(*):  </div></th>
<th rowspan="2"><div align="left">
  <input type="text"  name="End_DateF"   id="End_DateF" class="XBar-3" value="<?php echo $_POST['End_DateF'] ?>" >
</div></th>
<th rowspan="2"><div align="left">
  <input type="button"    value="Submit" class="XBar-BT" onClick="submitA()">
 <!-- <input type="button"   value="Export" class="XBar-BT" onClick="Export()" > !---->
</div></th>
<th colspan="4"><h style="color:gray; font-size:12px;">
  <div align="left">*Dept.  品保:1 製造:2</div>
</h></th>
</tr>
<tr>
  <th colspan="4"><h style="color:gray; font-size:12px">
    <div align="left">*Station  生胚:1 燒結:2 電鍍:3 CNC:4 研磨:5 噴塗:6</div>
  </h></th>
</tr>
</table>

</form>
</body>
</html>

<?php
//mysql_free_result($Project_NameF);
//mysql_free_result($Mold_NumberF);
mysql_query("SET NAMES utf8");
?>
