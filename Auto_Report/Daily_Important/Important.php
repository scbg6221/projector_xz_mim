<?php
//require_once '../../../../Public/Connections/projector_system_MIM.php';
require_once '../../../../Public/Connections/projector_system_XZ_MIM_web.php';
require_once '../../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once '../../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';

//2維array排序的函數
function score_sort($a, $b)
{
    //下面的FAI_Number是要排序的陣列索引
    if ($a['1'] == $b['1']) {
        return 0;
    }
    return ($a['1'] > $b['1']) ? 1 : -1;
}

//尺寸報表打包
ini_set("memory_limit", "-1");
ini_set("max_execution_time", '0');
$d2 = date("Y-m-d");
$d1 = date('Y-m-d', strtotime($d2) - 60 * 60 * 24);
//$d2 = '2017-08-04';
//$d1 = '2017-08-03';
//error_reporting(0);

//$queryMold = "SELECT * FROM `mim_main_table` WHERE  CONCAT( End_Date, '-', End_time ) BETWEEN '" . $d1 . "-08:00:00' AND '" . $d2 . "-08:00:00' AND Spec_Status = 'Important' AND Spec_Status!='IQC' group by Mold_Number,Part_Number_V,Spec_Status,Status,Station,Department order by Part_Number_V,Mold_Number";
$queryMold = "SELECT * FROM `mim_main_table` WHERE  CONCAT( End_Date, '-', End_time ) BETWEEN '" . $d1 . "-08:00:00' AND '" . $d2 . "-08:00:00' AND Spec_Status!='IQC' group by Mold_Number,Part_Number_V,Spec_Status,Status,Station,Department order by Part_Number_V,Mold_Number";

echo $queryMold;
mysqli_select_db($connect, $database);
$ResultMold = mysqli_query($connect, $queryMold);
while ($ResultMoldWhile = mysqli_fetch_assoc($ResultMold)) {
    $SheetLimit   = 12;
    $OP_Night     = $OP_Day     = '';
    $Remark       = array();
    $Project_Name = $ResultMoldWhile['Project_Name'];
    $cc           = $ResultMoldWhile['Part_Number_V'];
    $bb           = $ResultMoldWhile['Mold_Number'];
    $Spec_Status  = $ResultMoldWhile['Spec_Status'];
    $End_Date     = $ResultMoldWhile['End_Date'];
    $Furnace_M    = $ResultMoldWhile['Furnace_M'];
    $Furnace_No   = $ResultMoldWhile['Furnace_No'];
    $Molding_M    = $ResultMoldWhile['Molding_M'];
    $Status       = $ResultMoldWhile['Status'];
    $Station      = $ResultMoldWhile['Station'];
    $Department   = $ResultMoldWhile['Department'];

    $queryTicket = "SELECT (SELECT count(Ticket_Number) FROM  `mim_main_table` WHERE  Mold_Number =  '" . $ResultMoldWhile['Mold_Number'] . "' AND Part_Number_V = '" . $ResultMoldWhile['Part_Number_V'] . "'  AND Department = '" . $ResultMoldWhile['Department'] . "' AND CONCAT( End_Date,  '-', End_time ) BETWEEN  '" . $d1 . "-08:00:00'AND  '" . $d2 . "-08:00:00') as count,Ticket_Number,Cavity,End_Time,End_Date,Remark,Status FROM  `mim_main_table` WHERE  Mold_Number =  '" . $ResultMoldWhile['Mold_Number'] . "' AND Part_Number_V = '" . $ResultMoldWhile['Part_Number_V'] . "'  AND Department = '" . $ResultMoldWhile['Department'] . "' AND CONCAT( End_Date,  '-', End_time ) BETWEEN  '" . $d1 . "-08:00:00'AND  '" . $d2 . "-08:00:00' order by CONCAT( End_Date, '-', End_time )";
    //echo $queryTicket . "<br>";

    /*
    $SheetLimit   = 12;
    $OP_Night     = $OP_Day     = '';
    $Project_Name = 'G1 Snap 117-1018-10';
    $cc           = '8101-0050-8723';
    $bb           = '2213';
    $Spec_Status  = 'CPK';
    $End_Date     = '2017-08-10';
    $Furnace_M    = "#3";
    $Furnace_No   = "115";
    $Molding_M    = "4";
    $Status       = '2';
    $Station      = '1';
    $Department   = '1';

    $queryTicket = "SELECT (SELECT count(Ticket_Number) FROM  `mim_main_table` WHERE  Mold_Number =  '" . $bb . "' AND Part_Number_V = '" . $cc . "' AND Spec_Status = '" . $Spec_Status . "' AND Department = '" . $Department . "' AND CONCAT( End_Date,  '-', End_time ) BETWEEN  '" . $d1 . "-08:00:00'AND  '" . $d2 . "-08:00:00') as count,Ticket_Number,Cavity,End_Time,End_Date FROM  `mim_main_table` WHERE  Mold_Number =  '" . $bb . "' AND Part_Number_V = '" . $cc . "' AND Spec_Status = '" . $Spec_Status . "' AND Department = '" . $Department . "' AND CONCAT( End_Date,  '-', End_time ) BETWEEN  '" . $d1 . "-08:00:00'AND  '" . $d2 . "-08:00:00' order by CONCAT( End_Date, '-', End_time )";
    echo $queryTicket;
     */

    $t            = 0;
    $measure_time = 0;
    $DataAll      = array();
    mysqli_select_db($connect, $database);
    $QTicket = mysqli_query($connect, $queryTicket);
    while ($ResultTicket = mysqli_fetch_assoc($QTicket)) {
        $measure_time++;
        $Remark[]     = $ResultTicket['Remark'];
        $Time         = $ResultTicket['End_Time'];
        $Ticket       = $ResultTicket['Ticket_Number'];
        $count        = $ResultTicket['count'];
        $Cavity_table = 'mim_measure_data_' . $ResultTicket['Cavity'] . 'cav';
        $QData        = "SELECT * FROM " . $Cavity_table . " WHERE Ticket_Number = '" . $ResultTicket['Ticket_Number'] . "' order by `Position`,`FAI_Number`,`Cavity_Number`";
        //echo $QData;
        //echo "=====================<br>";
        //echo $Ticket . "<br>";
        unset($DataAll);
        mysqli_select_db($connect, $database);
        $ResultData = mysqli_query($connect, $QData);

        while ($Result = mysqli_fetch_assoc($ResultData)) {

            $Personal_ID = $Result['Personal_ID'];
            $cav         = $ResultTicket['Cavity'];

            if ($Result['Measure_Datetime'] == '0000-00-00 00:00:00') {
                $MeasureValue     = '';
                $Measure_Datetime = '';
                $Measure_Result   = '';
            } //if($Result['Measure_Datetime']=='0000-00-00 00:00:00')
            else {
                $MeasureValue     = $Result['Measure_Value'];
                $Measure_Datetime = $Result['Measure_Datetime'];
                $Measure_Result   = $Result['Measure_Result'];
            } //else
            if ($t == 0) //the beginning of a FAI
            {
                unset($Data_Array);
                unset($Data_Array1);
                unset($Data_Array2);
                $Data_Array         = array();
                $MeasureValue_Array = array();

                //將FAI號碼後面串Position
                if ($Result['Position'] == '') {
                    $FAI_Number = $Result['FAI_Number'];
                } else {
                    $FAI_Number = $Result['FAI_Number'] . "_" . $Result['Position'];
                }
                //$New_FAI_Number = DimNO_OrderName($FAI_Number); //將FAI_Number補0
                $Data_Array = array($Time, $FAI_Number, $Result['Nominal_Dim'], $Result['Upper_Dim'], $Result['Lower_Dim']);

            } //if($t==0)
            //echo $MeasureValue;
            array_push($Data_Array, $MeasureValue);

            if ($MeasureValue != "") {
                array_push($MeasureValue_Array, $MeasureValue);
            }
            //print_r($MeasureValue_Array);
            $t++;

            if ($t == ($cav)) {
                $last = 5 + $cav;
                $OK   = 0;
                $Null = 0;

                //計算Upper_Percentage & Lower_Percentage
                @$Max_MeasureValue = max($MeasureValue_Array);
                @$Min_MeasureValue = min($MeasureValue_Array);
                $Avg               = ($Data_Array[3] + $Data_Array[4]) / 2;
                //echo $Max_MeasureValue;
                if ($Max_MeasureValue == 0 & $Min_MeasureValue == 0) {
                    $Upper_Percentage = '';
                    $Lower_Percentage = '';
                } else {
                    if ($Data_Array[4] == 0) {
                        //$Upper_Percentage = round($Max_MeasureValue * 100 / $Data_Array[3]);
                        //$Lower_Percentage = round($Min_MeasureValue * 100 / $Data_Array[3]);
                        @$Upper_Percentage = $Max_MeasureValue * 100 / $Data_Array[3];
                        @$Lower_Percentage = $Min_MeasureValue * 100 / $Data_Array[3];
                    } else {
                        //$Upper_Percentage = round(($Max_MeasureValue - $Avg) * 100 / (($Data_Array[3] - $Data_Array[4]) / 2));
                        //$Lower_Percentage = round(($Min_MeasureValue - $Avg) * 100 / (($Data_Array[3] - $Data_Array[4]) / 2));
                        @$Upper_Percentage = ($Max_MeasureValue - $Avg) * 100 / (($Data_Array[3] - $Data_Array[4]) / 2);
                        @$Lower_Percentage = ($Min_MeasureValue - $Avg) * 100 / (($Data_Array[3] - $Data_Array[4]) / 2);
                    }
                }
                //echo "Upper_Percentage=" . $Upper_Percentage;
                //echo "Lower_Percentage=" . $Lower_Percentage . "<br>";

                for ($i = 5; $i < $last; $i++) {
                    if ($Data_Array[$i] <= $Data_Array[3] & $Data_Array[$i] >= $Data_Array[4] or $Data_Array[$i] == '') {
                        $OK++;
                    }
                    if ($Data_Array[$i] == '') {
                        $Null++;
                    }

                } //for($i=4;$i<$last;$i++)

                if (count($Data_Array) > 13) {
                    $Data_Array1 = array_slice($Data_Array, 0, 13);
                    $Data_Array2 = array_slice($Data_Array, 13);
                    array_unshift($Data_Array2, "", "", "", "", "");

                } //if(count($Data_Array)>15)
                else {
                    $Data_Array1 = $Data_Array;
                }
                //echo "OK:" . $OK . "<br>";
                //echo "Null:" . $Null . "<br>";
                //echo "Upper_Percentage:" . $Upper_Percentage . "<br>";
                //echo "Lower_Percentage:" . $Lower_Percentage . "<br>";
                if ($Null == $cav) {
                    array_push($Data_Array1, "", $Upper_Percentage, $Lower_Percentage);
                } else if ($OK == $cav & $Null != $cav) {
                    array_push($Data_Array1, "OK", $Upper_Percentage, $Lower_Percentage);
                } else {
                    array_push($Data_Array1, "NG", $Upper_Percentage, $Lower_Percentage);
                }
                //print_r($Data_Array1) . "<br>";
                $DataAll[] = $Data_Array1;
                if (count($Data_Array) > 13) {
                    $DataAll[] = $Data_Array2;
                }
                /*
                if ($Station != 2) {
                usort($DataAll, 'score_sort'); //將2維array利用FAI_Number欄位做排序
                }
                 */

                //print_r($DataAll) . "<br>";
                $t = 0;
            } //if($t==($cav))
        } //while($Result = mysqli_fetch_assoc($ResultData))

        $Info = "SELECT * FROM `mim_spec_main_table` WHERE `Part_Number_V` ='" . $cc . "'";
        mysqli_select_db($connect, $database);
        $InformationF2 = mysqli_query($connect, $Info);
        $Information2  = mysqli_fetch_assoc($InformationF2);
        $Material_Spec = $Information2['Material_Spec'];

        $index = 1;
        if ($cav < 8) {
            $row = 6;
            $num = 45;
        } else {
            $row = 6;
            $num = 48;
        }

        if ($cav == 1) {
            $Template = "1cav.xlsx";
        } else if ($cav == 2) {
            $Template = "2cav.xlsx";
        } else if ($cav == 4) {
            $Template = "4cav.xlsx";
        } else if ($cav == 5) {
            $Template = "5cav.xlsx";
        } else if ($cav == 6) {
            $Template = "6cav.xlsx";
        } else {
            $Template = "8cav.xlsx";
        }

        //創建資料夾($bb is mold number)
        //$path = "../../../../Report/Projector_System/HQ_MIM/Daily_Report/" . $d1 . "/" . $bb . "/";
        $path = "../../../../Report/Projector_System/HQ_MIM/Daily_Report/" . $Project_Name . "/" . $Furnace_No . "/";
        //echo $path;
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        //定義報告檔名
        if ($Department == 1) {
            $Department_Name = "品保";
        } else if ($Department == 2) {
            $Department_Name = "製造";
        }

        $station_array = array("生胚" => "1", "燒結" => "2", "整形" => "3", "電鍍" => "4", "燒結+噴砂" => "5", "研磨" => "6", "噴砂" => "7", "CNC" => "8", "噴漆" => "9", "髮絲" => "10", "鈍化" => "11", "PVD" => "12");
        $Station_Name  = array_search($Station, $station_array);

        $Department_Name = iconv("utf-8", "big5", $Department_Name);
        $Station_Name    = iconv("utf-8", "big5", $Station_Name);

        if ($Spec_Status == 'ALL') {
            $Type          = iconv("utf-8", "big5", "全尺寸");
            $filename_xlsx = $Type . "_" . $Department_Name . "_" . $bb . "_" . $Furnace_No . "_" . $cc . "_" . $Station_Name . "_" . date('Ymd', strtotime($d1)) . ".xlsx";
        } else if ($Spec_Status == 'IQC') {
            $Type          = iconv("utf-8", "big5", "出貨報告");
            $filename_xlsx = $Type . "_" . $Department_Name . "_" . $bb . "_" . $Furnace_No . "_" . $cc . "_" . $Station_Name . "_" . date('Ymd', strtotime($d1)) . ".xlsx";
        } else {
            $filename_xlsx = $Department_Name . "_" . $bb . "_" . $Furnace_No . "_" . $cc . "_" . $Station_Name . "_" . date('Ymd', strtotime($d1)) . ".xlsx";
        }

        //判別檔案是否存在
        $filepath = $path . "/" . $filename_xlsx;
        //$newfilepath = "../../../../Report/Projector_System/HQ_MIM/Daily_Report/" . $d1 . "/" . $filename_xlsx;
        $newfilepath = "../../../../Report/Projector_System/HQ_MIM/Daily_Report/" . $Project_Name . "/" . $filename_xlsx;

        if ($count > 1) {
            if (file_exists($newfilepath)) {
                $load_report = PHPExcel_IOFactory::load($newfilepath);
            } else {
                $load_report = PHPExcel_IOFactory::load($Template);
            }
        } else {
            $load_report = PHPExcel_IOFactory::load($Template);
        }

        $tt = ceil(count($DataAll) / $num);

        for ($pp = 1; $pp <= $tt; $pp++) {
            $prev_value = "";
            if ($cav < 8) {
                if (($pp != 1) and ($pp % 2 == 1)) {
                    $index++;
                }

                if ($pp % 2 == 0) {
                    $letter = 'M';
                } else {
                    $letter = 'A';
                }

            } else {
                if ($pp > 1) {
                    $index++;
                }

                $letter = 'A';
            }
            if ($pp != 1) {
                $arrayData = array_slice($DataAll, $num * ($pp - 1), $num);
            } elseif ($pp == 1) {
                $arrayData = array_slice($DataAll, 0, $num);
            }
            //echo $measure_time . "<br>";
            $sheetname_array = array("初件" => 1, "巡檢" => 2);
            $sheetname       = array_search($measure_time, $sheetname_array);
            //echo $sheetname . '-' . $index . "<br>";

            //$load_report->setActiveSheetIndex($index);
            $load_report->setActiveSheetIndexByName($sheetname . '-' . $index);
            $get_activesheet = $load_report->getActiveSheet();
            if ($arrayData) {
                $get_activesheet->fromArray($arrayData, null, $letter . $row);
                //print_r($arrayData);
            }

            //依據Station抓取$Machine_No
            if ($Station == 1) {
                $Machine_No = $Molding_M;
            } else if ($Station == 2) {
                $Machine_No = $Furnace_M;
            }

            //print_r($Remark) . "<br>";
            $Part_Number = substr($cc, 0, 14);
            $Cad_Rev     = substr($cc, 15);
            $Mold_Number = $bb;
            $get_activesheet->setCellValue('C2', $Part_Number);
            $get_activesheet->setCellValue('C3', $Project_Name);
            $i  = 0;
            $yy = count($Remark) + 5;

            if ($cav == 1) {
                $get_activesheet->setCellValue('G2', $Mold_Number);
                $get_activesheet->setCellValue('G3', $End_Date);
                $get_activesheet->setCellValue('M2', $Ticket);
                $get_activesheet->setCellValue('M3', $Machine_No);
                $get_activesheet->setCellValue('P2', $Cad_Rev);
                $get_activesheet->setCellValue('P3', $Material_Spec);
                $get_activesheet->setCellValue('N51', $Personal_ID);

                if ($pp % 2 == 1 && $Remark[$measure_time - 1] != "") {

                    $get_activesheet->setCellValue('S6', $Remark[$measure_time - 1]);
                }

            } else if ($cav == 2) {
                $get_activesheet->setCellValue('G2', $Mold_Number);
                $get_activesheet->setCellValue('G3', $End_Date);
                $get_activesheet->setCellValue('O2', $Ticket);
                $get_activesheet->setCellValue('O3', $Machine_No);
                $get_activesheet->setCellValue('R2', $Cad_Rev);
                $get_activesheet->setCellValue('R3', $Material_Spec);
                $get_activesheet->setCellValue('O51', $Personal_ID);

                if ($pp % 2 == 1 && $Remark[$measure_time - 1] != "") {

                    $get_activesheet->setCellValue('U6', $Remark[$measure_time - 1]);
                }

            } else if ($cav == 4) {
                $get_activesheet->setCellValue('H2', $Mold_Number);
                $get_activesheet->setCellValue('H3', $End_Date);
                $get_activesheet->setCellValue('O2', $Ticket);
                $get_activesheet->setCellValue('O3', $Machine_No);
                $get_activesheet->setCellValue('U2', $Cad_Rev);
                $get_activesheet->setCellValue('U3', $Material_Spec);
                $get_activesheet->setCellValue('Q51', $Personal_ID);

                if ($pp % 2 == 1 && $Remark[$measure_time - 1] != "") {

                    $get_activesheet->setCellValue('Y6', $Remark[$measure_time - 1]);
                }

            } else if ($cav == 5) {
                $get_activesheet->setCellValue('G2', $Mold_Number);
                $get_activesheet->setCellValue('G3', $End_Date);
                $get_activesheet->setCellValue('J2', $Ticket);
                $get_activesheet->setCellValue('J3', $Machine_No);
                $get_activesheet->setCellValue('M2', $Cad_Rev);
                $get_activesheet->setCellValue('M3', $Material_Spec);
                $get_activesheet->setCellValue('L51', $Personal_ID);

                if ($pp % 2 == 1 && $Remark[$measure_time - 1] != "") {

                    $get_activesheet->setCellValue('N6', $Remark[$measure_time - 1]);
                }

            } else if ($cav == 6 or $cav == 8) {
                $get_activesheet->setCellValue('G2', $Mold_Number);
                $get_activesheet->setCellValue('G3', $End_Date);
                $get_activesheet->setCellValue('J2', $Ticket);
                $get_activesheet->setCellValue('J3', $Machine_No);
                $get_activesheet->setCellValue('N2', $Cad_Rev);
                $get_activesheet->setCellValue('N3', $Material_Spec);
                $get_activesheet->setCellValue('L51', $Personal_ID);
                if ($cav == 6) {

                    if ($pp % 2 == 1 && $Remark[$measure_time - 1] != "") {

                        $get_activesheet->setCellValue('O6', $Remark[$measure_time - 1]);
                    }

                } else {

                    if ($pp % 2 == 1 && $Remark[$measure_time - 1] != "") {

                        $get_activesheet->setCellValue('Q6', $Remark[$measure_time - 1]);
                    }

                }
            } else if ($cav == 16) {
                $get_activesheet->setCellValue('G2', $Mold_Number);
                $get_activesheet->setCellValue('G3', $End_Date);
                $get_activesheet->setCellValue('J2', $Ticket);
                $get_activesheet->setCellValue('J3', $Machine_No);
                $get_activesheet->setCellValue('M2', $Cad_Rev);
                $get_activesheet->setCellValue('M3', $Material_Spec);
                $get_activesheet->setCellValue('K51', $Personal_ID);

            }

            //填寫外觀頁表頭資訊
            $load_report->setActiveSheetIndexByName('外觀頁');
            $get_activesheet = $load_report->getActiveSheet();
            $get_activesheet->setCellValue('B3', $Part_Number);
            $get_activesheet->setCellValue('B4', $Project_Name);
            $get_activesheet->setCellValue('F3', $Mold_Number);
            $get_activesheet->setCellValue('F4', $End_Date);
            $get_activesheet->setCellValue('J3', $Ticket);
            $get_activesheet->setCellValue('J4', $Machine_No);
            $get_activesheet->setCellValue('N3', $Cad_Rev);
            $get_activesheet->setCellValue('N4', $Material_Spec);

            //填寫製程巡迴檢查表
            $load_report->setActiveSheetIndexByName('製程巡迴檢查表');
            $get_activesheet = $load_report->getActiveSheet();
            $get_activesheet->setCellValue('E2', $cc);
            $get_activesheet->setCellValue('H2', $Project_Name);
            $get_activesheet->setCellValue('C2', $End_Date);

            for ($y = 5; $y < $yy; $y++) {
                if ($Remark[$y - 5]) {
                    $get_activesheet->setCellValue('H' . $y, $Remark[$y - 5]);
                }
            }

            $status_array = array("B" => "1", "C" => "2", "D" => "3", "E" => "4", "F" => "5", "G" => "6");
            $Status_P     = array_search($Status, $status_array);
            if ($measure_time == 1) {
                $get_activesheet->setCellValue('A5', $Time);
                $get_activesheet->setCellValue($Status_P . '5', 'V');

            } else if ($measure_time == 2) {
                $get_activesheet->setCellValue('A6', $Time);
                $get_activesheet->setCellValue($Status_P . '6', 'V');
            }

            $objWriter = PHPExcel_IOFactory::createWriter($load_report, 'Excel2007');
            $objWriter->setIncludeCharts(true);
            $objWriter->setPreCalculateFormulas(false);
            //定義儲存位子
            if ($count > 1) {
                if (file_exists($newfilepath)) {
                    $objWriter->save($filepath);
                    unlink($newfilepath); //删除新目录下的文
                } else {
                    $objWriter->save($newfilepath);
                }
            } else {
                $objWriter->save($filepath);
            }
        }

    } //while($ResultTicket = mysqli_fetch_assoc($QTicket) )
}
