<?php
require_once '../../../../Public/Connections/projector_system_XZ_MIM_web.php';
//require_once '../../../../Public/Connections/projector_system_XZ_MIM_web.php';
//require_once '../../../../Public/Connections/projector_system_Oracle.php';
require_once '../../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once '../../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';

//2維array排序的函數
function score_sort($a, $b)
{
    //下面的FAI_Number是要排序的陣列索引
    if ($a['0'] == $b['0']) {
        return 0;
    }
    return ($a['0'] > $b['0']) ? 1 : -1;
}

//尺寸報表打包
ini_set("memory_limit", "-1");
ini_set("max_execution_time", '0');
$d2 = date("Y-m-d");
$d1 = date('Y-m-d', strtotime($d2) - 60 * 60 * 24);
$d2 = '2017-08-02';
$d1 = '2017-08-01';
//error_reporting(0);

$queryMold = "SELECT * FROM `mim_main_table` WHERE  CONCAT( End_Date, '-', End_time ) BETWEEN '" . $d1 . "-08:00:00' AND '" . $d2 . "-08:00:00' AND Spec_Status = 'IQC' group by Mold_Number,Part_Number_V,Spec_Status,Status,Station,Department order by Part_Number_V,Mold_Number";
//echo $queryMold;

mysqli_select_db($connect, $database);
$ResultMold = mysqli_query($connect, $queryMold);
while ($ResultMoldWhile = mysqli_fetch_assoc($ResultMold)) {
    $SheetLimit   = 12;
    $OP_Night     = $OP_Day     = '';
    $Project_Name = $ResultMoldWhile['Project_Name'];
    $cc           = $ResultMoldWhile['Part_Number_V'];
    $bb           = $ResultMoldWhile['Mold_Number'];
    $Spec_Status  = $ResultMoldWhile['Spec_Status'];
    $End_Date     = $ResultMoldWhile['End_Date'];
    $Furnace_M    = $ResultMoldWhile['Furnace_M'];
    $Molding_M    = $ResultMoldWhile['Molding_M'];
    $Status       = $ResultMoldWhile['Status'];
    $Station      = $ResultMoldWhile['Station'];
    $Department   = $ResultMoldWhile['Department'];

    $queryTicket = "SELECT Ticket_Number,Cavity,End_Time,End_Date,Furnace_No FROM  `mim_main_table` WHERE  Mold_Number =  '" . $ResultMoldWhile['Mold_Number'] . "' AND Part_Number_V = '" . $ResultMoldWhile['Part_Number_V'] . "'  AND Department = '" . $ResultMoldWhile['Department'] . "' AND CONCAT( End_Date,  '-', End_time ) BETWEEN  '" . $d1 . "-08:00:00'AND  '" . $d2 . "-08:00:00' order by CONCAT( End_Date, '-', End_time )";
    //echo $queryTicket;
    /*
    $SheetLimit   = 12;
    $OP_Night     = $OP_Day     = '';
    $Project_Name = 'D2';
    $cc           = '8116-1A00-9008_E';
    $bb           = '884-643N1';
    $Spec_Status  = 'IQC';
    $End_Date     = '2017-08-01';
    $Furnace_M    = "#3";
    $Furnace_No   = "203";
    $Molding_M    = "4";
    $Status       = '2';
    $Station      = '1';
    $Department   = '1';

    $queryTicket = "SELECT Ticket_Number,Cavity,End_Time,End_Date FROM  `mim_main_table` WHERE  Mold_Number =  '884-643N1' AND Part_Number_V = '8116-1A00-9008_E' AND Spec_Status = 'IQC' AND Department = '1' AND CONCAT( End_Date,  '-', End_time ) BETWEEN  '" . $d1 . "-08:00:00'AND  '" . $d2 . "-08:00:00' order by CONCAT( End_Date, '-', End_time )";

    echo $queryTicket;
     */
    $t = 0;
    mysqli_select_db($connect, $database);
    $QTicket = mysqli_query($connect, $queryTicket);
    while ($ResultTicket = mysqli_fetch_assoc($QTicket)) {

        $Time         = $ResultTicket['End_Time'];
        $Ticket       = $ResultTicket['Ticket_Number'];
        $Furnace_No   = $ResultTicket['Furnace_No'];
        $Cavity_table = 'mim_measure_data_' . $ResultTicket['Cavity'] . 'cav';
        $QData        = "SELECT * FROM " . $Cavity_table . " WHERE Ticket_Number = '" . $ResultTicket['Ticket_Number'] . "' order by `Position`,`FAI_Number`,`Cavity_Number`";

        unset($DataAll);
        mysqli_select_db($connect, $database);
        $ResultData = mysqli_query($connect, $QData);
        while ($Result = mysqli_fetch_assoc($ResultData)) {

            $Personal_ID = $Result['Personal_ID'];
            $cav         = $ResultTicket['Cavity'];
            if ($Result['Measure_Datetime'] == '0000-00-00 00:00:00') {
                $MeasureValue     = '';
                $Measure_Datetime = '';
                $Measure_Result   = '';
            } //if($Result['Measure_Datetime']=='0000-00-00 00:00:00')
            else {
                $MeasureValue     = $Result['Measure_Value'];
                $Measure_Datetime = $Result['Measure_Datetime'];
                $Measure_Result   = $Result['Measure_Result'];
            } //else
            if ($t == 0) //the beginning of a FAI
            {
                unset($Data_Array);
                unset($Data_Array1);
                unset($Data_Array2);
                $Data_Array         = array();
                $MeasureValue_Array = array();

                //將FAI號碼後面串Position
                if ($Result['Position'] == '') {
                    $FAI_Number = $Result['FAI_Number'];
                } else {
                    $FAI_Number = $Result['FAI_Number'] . "_" . $Result['Position'];
                }
                $Data_Array = array($FAI_Number, '', $Result['Nominal_Dim'], $Result['Lower_Dim'], $Result['Upper_Dim']);

            } //if($t==0)
            array_push($Data_Array, $MeasureValue);
            if ($MeasureValue != "") {
                array_push($MeasureValue_Array, $MeasureValue);
            }
            $t++;
            if ($t == ($cav)) {
                $last = 5 + $cav;
                $OK   = 0;
                $Null = 0;

                //計算Upper_Percentage & Lower_Percentage
                @$Max_MeasureValue = max($MeasureValue_Array);
                @$Min_MeasureValue = min($MeasureValue_Array);
                $Avg               = ($Data_Array[3] + $Data_Array[4]) / 2;

                if ($Max_MeasureValue == 0 & $Min_MeasureValue == 0) {
                    $Upper_Percentage = '';
                    $Lower_Percentage = '';
                } else {
                    if ($Data_Array[3] == 0) {
                        //$Upper_Percentage = round($Max_MeasureValue * 100 / $Data_Array[3]);
                        //$Lower_Percentage = round($Min_MeasureValue * 100 / $Data_Array[3]);
                        @$Upper_Percentage = $Max_MeasureValue * 100 / $Data_Array[4];
                        @$Lower_Percentage = $Min_MeasureValue * 100 / $Data_Array[4];
                    } else {
                        //$Upper_Percentage = round(($Max_MeasureValue - $Avg) * 100 / (($Data_Array[3] - $Data_Array[4]) / 2));
                        //$Lower_Percentage = round(($Min_MeasureValue - $Avg) * 100 / (($Data_Array[3] - $Data_Array[4]) / 2));
                        @$Upper_Percentage = ($Max_MeasureValue - $Avg) * 100 / (($Data_Array[4] - $Data_Array[3]) / 2);
                        @$Lower_Percentage = ($Min_MeasureValue - $Avg) * 100 / (($Data_Array[4] - $Data_Array[3]) / 2);
                    }
                }
                //echo "Upper_Percentage=" . $Upper_Percentage;
                //echo "Lower_Percentage=" . $Lower_Percentage . "<br>";

                for ($i = 5; $i < $last; $i++) {
                    if ($Data_Array[$i] <= $Data_Array[4] & $Data_Array[$i] >= $Data_Array[3] or $Data_Array[$i] == '') {
                        $OK++;
                    }
                    if ($Data_Array[$i] == '') {
                        $Null++;
                    }

                } //for($i=4;$i<$last;$i++)
                if (count($Data_Array) > 13) {
                    $Data_Array1 = array_slice($Data_Array, 0, 13);
                    $Data_Array2 = array_slice($Data_Array, 13);
                    array_unshift($Data_Array2, "", "", "", "", "");

                } //if(count($Data_Array)>15)
                else {
                    $Data_Array1 = $Data_Array;
                }

                if ($Null == $cav) {
                    array_push($Data_Array1, "", $Upper_Percentage, $Lower_Percentage);
                } else if ($OK == $cav & $Null != $cav) {
                    array_push($Data_Array1, "PASS", $Upper_Percentage, $Lower_Percentage);
                } else {
                    array_push($Data_Array1, "FAIL", $Upper_Percentage, $Lower_Percentage);
                }

                $DataAll[] = $Data_Array1;
                if (count($Data_Array) > 13) {
                    $DataAll[] = $Data_Array2;
                }
                usort($DataAll, 'score_sort'); //將2維array利用FAI_Number欄位做排序
                //print_r($DataAll) . "<br>";
                $t = 0;
            } //if($t==($cav))
        } //while($Result = mysqli_fetch_assoc($ResultData))

        $index = 1;
        $row   = 17;
        $num   = 13;

        if ($cav == 9) {
            $Template = "IQC_9cav.xlsx";
        } else {
            $Template = "IQC_8cav.xlsx";
        }

        //創建資料夾($bb is mold number)
        //$path = "../../../../Report/Projector_System/HQ_MIM/DailyReport/" . $d1 . "/" . $bb . "/";
        $path = "../../../../Report/Projector_System/HQ_MIM/IQC_Report/" . $Project_Name . "/" . $Furnace_No . "/";
        //echo $path;
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        //定義報告檔名
        if ($Department == 1) {
            $Department_Name = "品保";
        } else if ($Department == 2) {
            $Department_Name = "製造";
        }

        $station_array = array("生胚" => "1", "燒結" => "2", "整形" => "3", "電鍍" => "4", "燒結噴砂" => "5", "研磨" => "6", "噴砂" => "7", "CNC" => "8", "噴漆" => "9", "髮絲" => "10", "鈍化" => "11", "PVD" => "12");
        $Station_Name  = array_search($Station, $station_array);

        $Department_Name = iconv("utf-8", "big5", $Department_Name);
        $Station_Name    = iconv("utf-8", "big5", $Station_Name);

        $Type          = iconv("utf-8", "big5", "出貨報告");
        $filename_xlsx = $Type . "_" . $Department_Name . "_" . $bb . "_" . $Furnace_No . "_" . $cc . "_" . $Station_Name . "_" . date('Ymd', strtotime($d1)) . ".xlsx";

        $filepath    = $path . "/" . $filename_xlsx;
        $load_report = PHPExcel_IOFactory::load($Template);

        $tt = ceil(count($DataAll) / $num);

        for ($pp = 1; $pp <= $tt; $pp++) {

            if ($pp > 1) {
                $index++;
            }
            $letter = 'B';

            if ($pp != 1) {
                $arrayData = array_slice($DataAll, $num * ($pp - 1), $num);
            } elseif ($pp == 1) {
                $arrayData = array_slice($DataAll, 0, $num);
            }

            $load_report->setActiveSheetIndexByName('檢驗-' . $index);
            $get_activesheet = $load_report->getActiveSheet();
            if ($arrayData) {
                $get_activesheet->fromArray($arrayData, null, $letter . $row);
                //print_r($arrayData);
            }

            //依據Station抓取$Machine_No
            if ($Station == 1) {
                $Machine_No = $Molding_M;
            } else if ($Station == 2) {
                $Machine_No = $Furnace_M;
            }

            $Part_Number = substr($cc, 0, 14);
            $Cad_Rev     = substr($cc, 15);
            $Mold_Number = $bb;

            $get_activesheet->setCellValue('E10', $Part_Number);
            $get_activesheet->setCellValue('E8', $Project_Name);
            $get_activesheet->setCellValue('C6', $End_Date);
            $get_activesheet->setCellValue('I10', $Cad_Rev);
            $get_activesheet->setCellValue('M36', $Personal_ID);

            $objWriter = PHPExcel_IOFactory::createWriter($load_report, 'Excel2007');
            $objWriter->setIncludeCharts(true);
            $objWriter->setPreCalculateFormulas(false);
            $objWriter->save($filepath);
        }
    } //while($ResultTicket = mysqli_fetch_assoc($QTicket) )
}
